import os
import openai
from openai import OpenAIError
import anthropic
import litellm
from litellm import completion
import json
import logging
import subprocess
from datetime import datetime
import time
import re
from selenium import webdriver
from selenium.webdriver.common.by import By
from litellm import embedding
import pinecone
import threading
from fastapi import FastAPI
import shutil
from fastapi import FastAPI, Body, BackgroundTasks, HTTPException, Depends, Header
from peewee import *
from starlette.status import HTTP_401_UNAUTHORIZED
from fastapi import HTTPException
from starlette.background import BackgroundTasks
from starlette.status import HTTP_404_NOT_FOUND
import requests
import string
import random
import psycopg2
from psycopg2 import sql
from slugify import slugify
from dotenv import load_dotenv
import uuid
from datetime import datetime, timezone
import os
from enum import Enum
from tortoise.contrib.fastapi import register_tortoise
from tortoise import fields
from tortoise.models import Model
import firebase_admin
from firebase_admin import credentials, auth
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi import Request
import asyncio
from fastapi.responses import JSONResponse
from tortoise import run_async
from fastapi.middleware.cors import CORSMiddleware
import asyncio
import sys
from tortoise import Tortoise
from tortoise import run_async
from tortoise.transactions import in_transaction
import threading
import queue
import traceback
import logging
import psycopg2
from logging import Handler
from contextlib import contextmanager
from loguru import logger
from contextvars import ContextVar


load_dotenv()

app = FastAPI()


# =========================
# Logging Config
# =========================

@app.middleware("http")
async def log_requests(request, call_next):
    id = request.headers.get('x-request-id', '/')
    logger.info(f"rid={id} start request path={request.url.path}")
    response = await call_next(request)
    logger.info(f"rid={id} completed request path={request.url.path} status_code={response.status_code}")
    return response

@contextmanager
def get_db_connection():
    conn = None
    try:
        conn = psycopg2.connect(os.getenv('DATABASE_CONNECTION_STRING'))
        yield conn
    except psycopg2.DatabaseError as e:
        logger.error(f"Database connection error: {e}")
        raise
    finally:
        if conn is not None:
            conn.close()

# logger.add(f"/root/softgen.ai/logs/{{time:YYYY-MM-DD}}.log", rotation="1 day", level="INFO")

# Modify the setup_task_logger function to pass the request_id to the db_sink
def setup_task_logger(project_id, project_task_id, request_id):
    logger.add(lambda message: db_sink(message, project_id, project_task_id, request_id), level="INFO")

# Modify the db_sink function to include the request_id in the log message
def db_sink(message, project_id, project_task_id, request_id):
    log_entry = f"{message.record['time']} - {message.record['level']} - {request_id} - {message.record['message']}\n"
    with get_db_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("""
                UPDATE tasks 
                SET log_file = CONCAT(COALESCE(log_file, ''), %s) 
                WHERE project_id = %s AND project_task_id = %s
                """, 
                (log_entry, project_id, project_task_id))
            conn.commit()


# =========================
# LLM
# =========================

total_tokens_used = {
    "prompt_tokens": 0,
    "completion_tokens": 0,
    "total_tokens": 0,
    "total_cost": 0.0
}

def calculate_cost(usage):
    prompt_token_cost = 0.01 / 1000
    completion_token_cost = 0.03 / 1000
    prompt_tokens_cost = usage['prompt_tokens'] * prompt_token_cost
    completion_tokens_cost = usage['completion_tokens'] * completion_token_cost
    total_cost = prompt_tokens_cost + completion_tokens_cost
    return total_cost


def make_llm_api_call(messages, json_output=False, useSmallerModel=False, usePrimary=True, useSecondary=False, useTertiary=False, temperature=0, max_tokens=None):
    global total_tokens_used

    def attempt_api_call(api_call_func, max_attempts=3):
        for attempt in range(max_attempts):
            try:
                response = api_call_func()
                total_tokens_used['prompt_tokens'] += response['usage']['prompt_tokens']
                total_tokens_used['completion_tokens'] += response['usage']['completion_tokens']
                total_tokens_used['total_tokens'] += response['usage']['total_tokens']
                response_content = response.choices[0].message['content'] if json_output else response
                # Move the JSON validation check inside the try block
                if json_output:
                    if not is_valid_json(response_content):
                        logger.info(f"Invalid JSON received, retrying attempt {attempt + 1}")
                        continue
                    else:
                        return response
                else:
                    return response
            except OpenAIError as e:
                if e.error['code'] == 'rate_limit_exceeded':
                    logger.info("Rate limit exceeded, redirecting to secondary API.")
                    usePrimary = False
                    useSecondary = True
                    break
                logger.info(f"API call failed, retrying attempt {attempt + 1}. Error: {e}")
                time.sleep(5)
            except json.JSONDecodeError:
                logger.error(f"JSON decoding failed, retrying attempt {attempt + 1}")
                time.sleep(5)
        raise Exception("Failed to make API call after multiple attempts.")
        
    if usePrimary:  # OpenAI Standard
        os.getenv("OPENAI_API_KEY")
        def openai_api_call():
            return completion(
                model="gpt-3.5-turbo-1106" if useSmallerModel else "gpt-4-1106-preview",
                messages=messages,
                temperature=temperature,
                response_format={"type": "json_object"} if json_output else None,
                **({"max_tokens": max_tokens} if max_tokens is not None else {})
            )
        return attempt_api_call(openai_api_call)

    if not usePrimary and useSecondary:  # Azure
        os.getenv("AZURE_API_KEY")
        os.getenv("AZURE_API_BASE") 
        os.getenv("AZURE_API_VERSION")
        def azure_api_call():
            return completion(
                "azure/Sg-main",
                messages=messages,
                temperature=temperature,
                **({"max_tokens": max_tokens} if max_tokens is not None else {})
            )
        return attempt_api_call(azure_api_call)

    if not useSecondary and useTertiary:  # Anthropic
        os.getenv("ANTHROPIC_API_KEY")
        def anthropic_api_call():
            return completion(
                "claude-instant-1.2" if useSmallerModel else "claude-2.1",
                messages=messages,
                temperature=temperature,
                **({"max_tokens": max_tokens} if max_tokens is not None else {})
            )
        return attempt_api_call(anthropic_api_call)

# =========================
# File Operations
# =========================

backend_types = "BackendControllers, BackendServices, BackendUtils, BackendRoutes, BackendMiddleware, app.js, schema.prisma" 
frontend_types = "FrontendPages, FrontendComponents, FrontendHooks, FrontendStore, FrontendServices, FrontendUtils"  #FrontendStaticData, FrontendStyles

testing_types = "PlaywrightE2E"
file_types = backend_types + " " + frontend_types

excluded_files = ['.DS_Store', '.gitignore', 'package-lock.json', 'postcss.config.js', 'tailwind.config.js', 'next.config.js', 'playwright.config.js', 'jsconfig.json', 'components.json', 'API_doc.md', 'env.example']
excluded_dirs = ['cypress', 'cypress/downloads', 'node_modules', 'migrations', '.next', 'cypress/screenshots', 'playwright-report', 'test-results', 'dist', 'build', 'coverage' ] 
excluded_file_extensions = ['.ico', '.svg', '.png', '.jpg','.jpeg', '.gif', '.bmp', '.tiff', '.webp']

def get_directory_mapping():
    return {
        "BackendControllers": os.path.join(BACKEND_PROJECT_DIR, "src/controllers"),
        "BackendServices": os.path.join(BACKEND_PROJECT_DIR, "src/services"),
        "BackendUtils": os.path.join(BACKEND_PROJECT_DIR, "src/utils"),
        "BackendRoutes": os.path.join(BACKEND_PROJECT_DIR, "src/routes"),
        "BackendMiddleware": os.path.join(BACKEND_PROJECT_DIR, "src/middleware"),
        "app.js": BACKEND_PROJECT_DIR,
        "schema.prisma": os.path.join(BACKEND_PROJECT_DIR, "prisma"),
        "seed.js": os.path.join(BACKEND_PROJECT_DIR, "prisma"),
        "API_doc.md": ROOT_PROJECT_DIR,
        "PlaywrightE2E": os.path.join(TESTS_PROJECT_DIR, "tests"),        
        "FrontendPages": os.path.join(FRONTEND_PROJECT_DIR, "src/pages"),
        "FrontendComponents": os.path.join(FRONTEND_PROJECT_DIR, "src/components"),
        "FrontendApi": os.path.join(FRONTEND_PROJECT_DIR, "src/api"),
        "FrontendHooks": os.path.join(FRONTEND_PROJECT_DIR, "src/hooks"),
        "FrontendStore": os.path.join(FRONTEND_PROJECT_DIR, "src/store"),
        "FrontendUtils": os.path.join(FRONTEND_PROJECT_DIR, "src/utilities"),
        # "FrontendStaticData": os.path.join(FRONTEND_PROJECT_DIR, "src/staticData"),
        # "FrontendStyles": os.path.join(FRONTEND_PROJECT_DIR, "src/styles"),
    }

file_type_mapping = {
    "@controllers": "BackendControllers",
    "@middleware": "BackendMiddleware",
    "@routes": "BackendRoutes",
    "@services": "BackendServices",
    "@utils": "BackendUtils",
    "@api": "FrontendApi",
    "@components": "FrontendComponents",
    "@hooks": "FrontendHooks",
    "@utilities": "FrontendUtils",
    "@store": "FrontendStore"
}

def read_directory(directory):
    """Reads all files in a given directory and returns a file tree and file contents, excluding certain files and directories."""
    # logger.info(f"Reading directory {directory}")
    file_tree = []
    file_contents = {}

    for root, dirs, files in os.walk(directory):
        dirs[:] = [d for d in dirs if d not in excluded_dirs]
        files = [f for f in files if f not in excluded_files and not any(
            f.endswith(ext) for ext in excluded_file_extensions)]

        root_path = os.path.relpath(root, directory)
        file_tree.append({"directory": root_path, "files": files})

        for file in files:
            file_path = os.path.join(root, file)
            try:
                with open(file_path, 'r', encoding='utf-8') as file_content:
                    file_contents[file_path] = file_content.read()
            except UnicodeDecodeError:
                # print(f"Skipping non-UTF-8 encoded file: {file_path}")
                continue

    return file_tree, file_contents

def read_file_content(file_path):
    if os.path.exists(file_path):
        with open(file_path, 'r') as file:
            return file.read()
    # logger.info(f"Reading file content from {file_path}")
    return ""
    
def modify_file(file_path, new_content):
    # Ensure the directory exists
    os.makedirs(os.path.dirname(file_path), exist_ok=True)

    # Create or overwrite the file with the new content
    with open(file_path, 'w') as file:
        file.write(new_content)
    logger.info(f"Writing new content to {file_path}")

def rename_file(file_path, new_name):
    new_file_path = os.path.join(os.path.dirname(file_path), new_name)
    os.rename(file_path, new_file_path)
    logger.info(f"Renamed file {file_path} to {new_file_path}")

def delete_file(file_path):
    os.remove(file_path)
    logger.info(f"Deleted file {file_path}")

def get_file_path_by_name(file_name, directory):
    # Search for the file in the given directory
    for root, dirs, files in os.walk(directory):
        if file_name in files:
            return os.path.join(root, file_name)
    raise FileNotFoundError(f"File {file_name} not found in {directory}.")

def get_file_path_by_type(file_name, file_type):

    dir_path = get_directory_mapping().get(file_type)
    if not dir_path:
        raise ValueError(f"Invalid file type: {file_type}")

    # Ensure the directory exists before returning the file path
    full_dir_path = os.path.join(dir_path, os.path.dirname(file_name))
    os.makedirs(full_dir_path, exist_ok=True)

    file_full_path = os.path.join(dir_path, file_name)
    if not os.path.exists(file_full_path):
        # Create the file
        open(file_full_path, 'a').close()
    return file_full_path

def is_valid_json(data):
    try:
        return json.loads(data)
    except json.JSONDecodeError:
        print(f"Error: Unable to parse JSON. Value: {data}")
        raise Exception("Failed to parse JSON.")

def extract_imports_from_file_contents(file_content, depth=0, max_depth=1):
    if depth > max_depth:
        return []
    imports = []
    for line in file_content.splitlines():
        if (('import ' in line or 'require(' in line) and any(x in line for x in ['@api', '@hooks', '@services', '@components', '@src', '@utils', '@utilities', '@store', '@controllers', '@middleware', '@routes', '@staticData', '/controllers', '/middleware', '/routes', '/services', '/utils', '/src', '/api', '/components', '/hooks', '/store'])):
            # Extract the file type and file name from the import line
            import_path = line.split("'")[1] if "'" in line else line.split('"')[1]
            file_type_key = import_path.split('/')[0]
            file_name_with_extension = import_path.split('/')[1]
            file_name = os.path.splitext(file_name_with_extension)[0]
            file_extension = os.path.splitext(file_name_with_extension)[1]
            # Check if the file has an extension, if not append .js or .jsx for FrontendComponents
            if not file_extension:
                if file_type_key == '@components':
                    file_name_with_extension += '.jsx'
                else:
                    file_name_with_extension += '.js'
            # Map the file type key to the actual file type
            file_type = file_type_mapping.get(file_type_key, "Unknown")
            # Construct the file path based on the file type and name
            file_path = get_file_path_by_type(file_name_with_extension, file_type)
            # Initialize the import entry
            import_entry = {
                "FileName": file_name_with_extension,  # Include the file extension in the file name
                "FileType": file_type,
                "FilePath": file_path,
                "FileContents": "",
                "FileImports": []
            }
            # Check if the file exists and read its contents
            if os.path.isfile(file_path):
                with open(file_path, 'r') as f:
                    file_content = f.read()
                import_entry["FileContents"] = file_content
                # Recursively extract imports from the file, increasing the depth
                import_entry["FileImports"] = extract_imports_from_file_contents(file_content, depth + 1, max_depth)
            # Append the import entry to the imports list
            imports.append(import_entry)
    print(imports)
    return imports 

def project_file_by_type_map(project_id, file_type_filter=None):
    set_global_project_info(project_id)
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    directory_mapping = get_directory_mapping()
    special_files = {
        "app.js": "app.js",
        "schema.prisma": "schema.prisma",
        "seed.js": "seed.js"
    }
    file_type_map = {file_type: [] for file_type in directory_mapping}
    # Include FrontendPageRoutes in the file_type_map
    if file_type_filter is None or file_type_filter == "FrontendPageRoutes":
        file_type_map["FrontendPageRoutes"] = get_frontend_page_routes()
    for type_name, file_name in special_files.items():
        if file_type_filter is None or file_type_filter == type_name:
            file_type_map[type_name].append(file_name)
    for file_type, dir_path in directory_mapping.items():
        if (file_type_filter is None or file_type_filter == file_type) and (file_type.startswith("Frontend") or file_type.startswith("Backend")):
            if os.path.isdir(dir_path):
                for root, dirs, files in os.walk(dir_path):
                    for file in files:
                        if file.endswith(('.js', '.jsx', '.ts', '.tsx')):
                            relative_file_path = os.path.relpath(os.path.join(root, file), dir_path)
                            file_type_map[file_type].append(relative_file_path)
    # Output the file_type_map in structured JSON format
    if file_type_filter:
        return json.dumps({file_type_filter: file_type_map[file_type_filter]}, indent=4)
    else:
        return json.dumps(file_type_map, indent=4)

def get_frontend_page_routes(): #TODO add gen_dynamic_value=True -> with get_dynamic_route
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID    
    frontend_pages_json = project_file_by_type_map(PROJECT_ID, "FrontendPages")
    frontend_pages = json.loads(frontend_pages_json)["FrontendPages"]
    page_routes = []
    for page in frontend_pages:
        if not page.startswith("_") and page.endswith((".js", ".jsx")):
            route = os.path.splitext(page)[0]  # Remove file extension
            subdirectory = os.path.dirname(route).replace(os.path.sep, "/")
            base_name = os.path.basename(route)
            if base_name == "index":  # Handle index files
                if subdirectory:  # Subdirectory index file should correspond to the subdirectory route
                    route = "/" + subdirectory
                else:
                    route = "/"  # Root index file should correspond to the base route '/'
            else:
                # Non-index files should include their subdirectory as a prefix
                if subdirectory:  # Add leading slash if there is a subdirectory
                    route = "/" + os.path.join(subdirectory, base_name).replace(os.path.sep, "/")
                else:
                    route = "/" + base_name
            # Handle dynamic routes by replacing [id], [name], [anyContent] with the number 1
            route = re.sub(r'\[(\w+)\]', r'[\1]', route)
            page_routes.append(route)
    # logger.info(f"Found the following page routes: {page_routes}")
    return page_routes

def generate_codebase_map(): #WIP
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    codebase_map = []
    # Map files in the given directories and walk through subdirectories, excluding excluded_dirs
    # Standalone append to the codebase_map for specific files

    special_files = {
        "app.js": os.path.join(BACKEND_PROJECT_DIR, "app.js"),
        "schema.prisma": os.path.join(BACKEND_PROJECT_DIR, "prisma", "schema.prisma"),
        "seed.js": os.path.join(BACKEND_PROJECT_DIR, "prisma", "seed.js")
    }
    for type_name, file_path in special_files.items():
        if os.path.isfile(file_path):
            with open(file_path, 'r') as f:
                file_content = f.read()
            imports = extract_imports_from_file_contents(file_content)
            # Append the file information to the codebase_map
            codebase_map.append({
                "FileName": type_name,
                "FileType": type_name,
                "FilePath": file_path,
                "FileContents": file_content,
                "Imports": imports
            })

    for type_name, dir_path in get_directory_mapping().items():
        if type_name.startswith("Frontend") or type_name.startswith("Backend"):
            if os.path.isdir(dir_path):
                for root, dirs, files in os.walk(dir_path):
                    dirs[:] = [d for d in dirs if d not in excluded_dirs]
                    for file in files:
                        if file.endswith('.js') or file.endswith('.jsx') or file.endswith('.ts') or file.endswith('.tsx'):
                            file_path = os.path.join(root, file)
                            with open(file_path, 'r') as f:
                                file_content = f.read()
                            # Extract imports and import file data
                            imports = extract_imports_from_file_contents(file_content)
                            codebase_map.append({
                                "FileName": file,
                                "FileType": type_name,
                                "FilePath": file_path,
                                "FileContents": file_content,
                                "FileImports": imports
                            })

    print(codebase_map)


    # Return custom ctags file tree

    # 

    return codebase_map, 

# =========================
# Core Task creation and processing
# =========================

def generate_task_list(user_request=None, system_message=None, user_message=None, task_list_file_types=file_types, task_list_message=None, skip_plan=True, RAG_type=False):
    """Generates an Task List."""

    if system_message is None and not skip_plan:
        system_message = ""

    if user_message is None and not skip_plan:
        user_message = ""

    if task_list_message is None:

        task_list_message="""
        Task List:
        - Modifiable File Types: """+ str(task_list_file_types)+"""
        - Choose the Operation for the task, you can only select "MODIFY", "RENAME" or "DELETE". "MODIFY" is used to both modify existing file contents and create new files. "DELETE" deletes the file. "RENAME" renames the file. 
        - FOR RENAME OPERATION: only state the new name in the instructions like for example "Instructions": "newName.js" NOTHING ELSE.
        - Give exact instructions for the interdependencies between different files, ensuring that they are imported correctly and verything works coherently together and considering that changes in one area might affect others.
        - Making clear, deterministic and detailed instruction statements. Every file instructions should be understandable as a standalone without the context of the instructions from the other files.
        - Every task will be executed and worked on in isolation, without any outside information of other tasks so be specific if the task has an interdependency. Every task should be possible to work on in isolation from the other tasks.
        - Do not make up any packages, technologies, or dependencies, instead always use real ones. Use well-known third-parties to cover more complex use cases.
        - Do not output tasks with 'no changes needed' or 'this is not a direct change' instructions, instead just don't include them in the JSON output at all.
        - "Type" is an ENUM so you can only define Type as """+ str(task_list_file_types)+""" – You are STRICTLY limited to these options.
        - Per Task and its respective File strongly follow the Imports (what is imported?) and Exports (what is exported with which params?) If a consequent task is dependent on another File´s Export - include the EXACT Import statement with correct params if applicable.
        - IMPORTANT! Rank the tasks based on their file dependencies using a logical evaluation process. To determine the correct order, start by identifying files that have no dependencies or are only depended upon by others. These should be ranked first. Then, proceed to files that depend on the already ranked files. Continue this process iteratively, ranking each file by the number of dependencies it has and the order in which its dependent files are ranked. This ensures that changes to dependent files are completed before working on tasks that require those files, maintaining a coherent update flow.
        - Strongly follow the given JSON Format – all instructions must be placed within the designated keys as presented in the example. Do not introduce any additional properties or alter the structure of the JSON output.
        - DO NOT create Tasks for any config files edits!
        - Only include the FileName in FileDependencies, DO NOT include the complete FilePath
        - If a FileName is in a nested folder within one of the file_types: """+ str(task_list_file_types)+""". Then YOU MUST INCLUDE THE RELATIVE PATH FROM THE FILE_TYPE ROOT for example folder1/fileName.js or folder1/folder2/fileName.jsx. THIS IS VERY IMPORTANT!
        - ONLY if a NPM package is MISSING specify their exact npm-package in FileDependencies so that it can be installed. ONLY include the npm package name nothing else, the <package-name> in 'npm install <package-name>'. MAKE SURE TO ONLY SPECIFY THE PACKAGE IF ITS MISSING, THAT MEANS IF YOU DO NOT SEE IT IN THE PACKAGE.JSON
        - Prioritize creating high-quality, well-structured files that centralize related functionalities. This approach emphasizes the importance of having fewer files, but with comprehensive and clear instructions for each. The goal is to achieve high cohesion within files, ensuring that all related components and functionalities are grouped together logically. At the same time, strive for low coupling between files, which simplifies maintenance and enhances the scalability of the codebase. By focusing on the quality of the files and the clarity of the instructions, the codebase becomes more intuitive and easier to work with for developers, leading to a more robust and efficient development process.

        You should deliver a comprehensive, actionable, and technically sound implementation plan in form of a Task List that addresses all aspects of integrating the user request – Tasks containing actionable development instructions the for individual Files in the codebase. Think deeply and step by step. Respond STRICTLY in this JSON Format. Output a JSON that contains the exact User Request as context, the Task List with all the Tasks consisting of the TaskID, the File related to the task and then the FileName, Type and Instructions for the file. DO NOT ADD ANY NEW KEYS OR CHANGE THE STRUCTURE.:

        {
            "Request": " The user request as general context for each task. ",
            "Task List": [
                {
                    "Task": {
                        "TaskID": " Index of the task based on procedural steps. IMPORTANT! Rank the tasks based on their file dependencies. It is crucial to ensure that if a task has a file dependency with a file being modified in another task, it is ranked correctly. This ensures that the changes to the dependent file are completed before working on the task that has the dependency. ",
                        "File": {
                            "FileName": " fileName.extension ",
                            "Type": " ENUM: (""" + str(task_list_file_types) + """) – You are STRICTLY limited to these options. ",
                            "Operation": " Select only one Operation from the ENUM: MODIFY,RENAME,DELETE ",                         
                            "Instructions": " FOR MODIFY OPERATION: Provide a detailed in-depth breakdown of what needs to be implemented in this file, give step-by-step instructions on what to do. Describe the implementation extensively – provide step by step instructions on what should be accomplished. Write up to as many words as needed. Make sure to state EXACT import & export Paths. The Instruction will be executed and worked on in isolation, without any outside information of other tasks so be specific if the task has an interdependency. Every task should be possible to work on in isolation from the other tasks so make sure to repeat yourself if needed including all the necessary information. The Instructions should be deterministic, clear and straight to the point. FOR RENAME OPERATION: only state the new name in the instructions. ONLY THE NEW NAME WITH FILE EXTENSION like for example "Instructions": "newName.js" NOTHING ELSE., else it will not rename as intended. FOR DELETE OPERATION: leave empty. ",
                            "FileDependencies": [ " <file1.js>", "<file2.jsx> ", " ... List all the files this particular file imports from within the regular directory structure. Only include the file names, not the full file paths. ", " <npm-package-name1>", "<npm-package-name2> ", " ... List all the npm packages that are not installed in the project yet. " ],                            
                        }
                    }
                }
            ]
        }
        """
        # "Commands" [ "npx prisma generate", "npm i <package_name>", "... any command needed " ],                            
        # "npm-not-installed-package-name1", "npm-not-installed-name2", "... ONLY if a NPM package is MISSING specify their exact npm-package name so that it can be installed. ONLY include the npm package name nothing else, the <package-name> in 'npm install <package-name>'. ONLY SPECIFY THE PACKAGE IF ITS MISSING." 


    messages = [
        {
            "role": "system",
            "content": system_message
        }
    ]


    messages.append({"role": "user", "content": user_message})


    # if RAG_type: 
    #     query_vector = generate_embeddings(user_request)
    #     identifiers = query_embeddings_from_pinecone(query_vector, "frontend", "chakra-pro-ui-components", top_k_value=10)
    #     rag_reference_contents = []
    #     for identifier in identifiers: 
    #         file_contents, complete_json_object = retrieve_storage_json_by_identifier(identifier, "template/chakra_contents.json")
    #         rag_reference_contents.append(file_contents)
    #     rag_reference_instructions = "Here are some premade Chakra PRO UI Components. Use them to build out the UX/UI. If you choose too use some of the files_provide the RagReference in the Task List with the specific file on our end. The RagReference is an UUID with this format: '2c9c1c0a-8ebc-4bae-9452-34e99e1a7d77_0'. " + str(rag_reference_contents)
    #     messages.append({"role": "user", "content": rag_reference_instructions})


    if not skip_plan:

        logger.info(f"Generating Implementation Plan")

        check_task_execution_stop(PROJECT_ID, TASK_ID)
        response = make_llm_api_call(messages)

        implementation_plan_contents = response.choices[0].message['content']
        logger.info(f"""Generated Implementation Plan: {
                     implementation_plan_contents}""")

        messages.append(
            {"role": "assistant", "content": implementation_plan_contents})

    messages.append({"role": "user", "content": task_list_message})

    check_task_execution_stop(PROJECT_ID, TASK_ID)
    logger.info(f"Generating Task List")
    response = make_llm_api_call(messages, json_output=True, max_tokens=4095)
    task_list_json = response.choices[0].message['content']

    logger.info(f"Task List generated: {task_list_json}")

    task_list_dict = json.loads(task_list_json)
    task_list_dict["Request"] = json.dumps(user_request)
    task_list_json = json.dumps(task_list_dict)
    

    return task_list_json

def process_task(task_list_json):
    global PROJECT_ID, TASK_ID

    tasks = is_valid_json(task_list_json)
    task_list = tasks.get("Task List", [])

    switch_git_branch(branch_name=f"task-{TASK_ID}")

    deploy_frontend = False
    deploy_backend = False

    for task in task_list:
        check_task_execution_stop(PROJECT_ID, TASK_ID)
        task_details = task["Task"]
        request = tasks.get("Request")
        file_info = task_details["File"]
        file_type = file_info["Type"]
        file_operation = file_info["Operation"]
        instructions = file_info["Instructions"]
        file_dependencies = file_info.get("FileDependencies", [])
        rag_reference = file_info.get("RagReference") 

        # Automatically set file_name for specific file_types
        if file_type == "seed.js":
            file_name = "seed.js"
        elif file_type == "schema.prisma":
            file_name = "schema.prisma"
        else:
            file_name = file_info["FileName"]
            # Strip away the prefix from the FileName if it is starting with it
            prefixes = ["pages/", "components/", "hooks/", "store/", "api/", "utils/", "utilities/", "styles/", "controllers/", "services/", "routes/", "middleware/", "src/", "src/pages/", "src/components/", "src/hooks/", "src/store/", "src/api/", "src/utils/", "src/utilities/", "src/styles/", "src/controllers/", "src/services/", "src/routes/", "src/middleware/"]
            for prefix in prefixes:
                if file_name.startswith(prefix):
                    file_name = file_name.removeprefix(prefix)
                    break

        logger.info(f"""Processing task for file: {file_name}, Type: {file_type}""")

        try:
            file_path = get_file_path_by_type(file_name, file_type)
        except ValueError as e:
            logger.error(f"Skipping task due to invalid file type: {e}")
            continue

        current_content = read_file_content(file_path)

        # logger.info(f"Current content of {file_name} retrieved")

        file_operation_lower = file_operation.lower()
        if file_operation_lower == "modify":
            for _ in range(3):
                try:
                    new_file_content = generate_code_newFileContents(
                        task, request, current_content)
                    logger.info(f"New content generated for {file_name}")

                    total_tokens_used['total_cost'] = calculate_cost(total_tokens_used)
                    logger.info(f"Total tokens used so far: {total_tokens_used}")

                    if isinstance(new_file_content['newFileContents'], dict):
                        modify_file(file_path, json.dumps(
                            new_file_content['newFileContents']))
                    else:
                        modify_file(file_path, new_file_content['newFileContents'])

                    logger.info(f"Modified file: {file_path}")

                    # If operation is successful, break the loop
                    break
                except json.decoder.JSONDecodeError:
                    # If operation fails, continue the loop to retry
                    continue
        elif file_operation_lower == "rename":
            new_name = instructions  # Assuming new name is provided in instructions
            rename_file(file_path, new_name)
            logger.info(f"Renamed file to: {new_name}")
        elif file_operation_lower == "delete":
            delete_file(file_path)
        else:
            raise ValueError(f"Unsupported file operation: {file_operation}")

        if file_type.startswith("Frontend"):
            deploy_frontend = True
        elif file_type.startswith("Backend") or file_type in ["app.js", "schema.prisma"]:
            deploy_backend = True

        # commit_and_push_changes_to_git(branch_name=f"task-{TASK_ID}", commit_message=f"{file_operation} {file_name}")
        logger.info("Completed process_task")

    commit_and_push_changes_to_git(branch_name=f"task-{TASK_ID}", commit_message=f"Task List completed with Magic")
    replace_main_branch(branch_name=f"task-{TASK_ID}")
    time.sleep(10)

def generate_code_newFileContents(task, request, current_content):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    task_details = task["Task"]
    file_info = task_details["File"]
    file_name = file_info["FileName"]
    file_type = file_info["Type"]
    instructions = file_info["Instructions"]
    file_dependencies = file_info.get("FileDependencies", [])
    rag_reference = file_info.get("RagReference") 
     
    # Dependency Context construction and NPM package installation
    dependency_file_content_instructions = ""
    file_dependencies_contents = {}
    npm_install_commands = []
    if file_dependencies:
        for dependency_file_name in file_dependencies:
            if '.' not in dependency_file_name:  # No extension, likely an NPM package
                # Exclude certain npm packages based on naming patterns
                if not any(excluded in dependency_file_name for excluded in ['@api', '@hooks', '@services', '@components', '@src', '@utilities', '@utils', '@store', '@controllers', '@middleware', '@routes', '@staticData', '/controllers', '/middleware', '/routes', '/services', '/utils', '/src', '/api', '/components', '/hooks', '/store']):
                    npm_install_commands.append(f"npm install {dependency_file_name}")
            else:
                search_dir = ROOT_PROJECT_DIR
                if file_type.startswith("Frontend"):
                    search_dir = FRONTEND_PROJECT_DIR
                elif file_type.startswith("Backend") or file_type in ["app.js", "schema.prisma"]:
                    search_dir = BACKEND_PROJECT_DIR
                if (dependency_file_name not in excluded_files and
                    os.path.splitext(dependency_file_name)[1] not in excluded_file_extensions and
                    not any(dependency_file_name.startswith(dir) for dir in excluded_dirs)):
                    try:
                        dependency_file_path = get_file_path_by_name(dependency_file_name, search_dir)
                        dependency_file_content = read_file_content(dependency_file_path)
                        file_dependencies_contents[dependency_file_name] = dependency_file_content
                    except FileNotFoundError:
                        logger.error(f"File {dependency_file_name} not found in {search_dir}. Continuing without this dependency.")
        if file_dependencies_contents:
            dependency_file_content_instructions = f"Here are the File Contents of files that {file_name} is dependent on <dependent_file_contents>{file_dependencies_contents}</dependent_file_contents>"
    if npm_install_commands:
        npm_install_command = " && ".join(npm_install_commands)
        try:
            if file_type.startswith("Frontend"):
                dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app/frontend && {npm_install_command}")
            elif file_type.startswith("Backend") or file_type in ["app.js", "schema.prisma"]:
                dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app/backend && {npm_install_command}")
            logger.info(f"NPM packages have been installed with command {npm_install_command}")
        except Exception as e:
            logger.error(f"An error occurred while installing NPM packages: {e}")
            raise

    be_file_tree, be_file_contents = read_directory(BACKEND_PROJECT_DIR)
    fe_file_tree, fe_file_contents = read_directory(FRONTEND_PROJECT_DIR)
    current_prisma_schema_content = read_file_content(os.path.join(BACKEND_PROJECT_DIR, "prisma", "schema.prisma"))
    current_seedjs_content = read_file_content(os.path.join(BACKEND_PROJECT_DIR, "prisma", "seed.js"))
    userjson_contents = read_file_content(os.path.join(ROOT_PROJECT_DIR, "users.json"))
    latest_curl_request_list_contents = read_file_content(os.path.join(ROOT_PROJECT_DIR, "cURL_requests.json"))
    
    # Add The descriptions from the main prompt for each to explain exactly what they should have content wise
    type_prompt = {
        "BackendControllers": f"You are working on a backend controller file.",
        "BackendServices": f"You are working on a backend service file. You should use the Prisma ORM Client to interact with the DB.",
        "BackendUtils": f"You are working on a backend util file.",
        "BackendMiddleware": f"You are working on a middleware file.",
        "BackendRoutes": f"You are working on a backend route file. Do not add '/api', only add /topic & the topic related routes. ",
        "app.js": f"You are working on app.js. Ensure that you do not duplicate the '/topic' route if 'topicRoutes' is already defined. The correct usage is: app.use('/api', topicRoutes); instead of app.use('/api/topic', topicRoutes);. Duplicating and adding '/topic' to the app.use will result in an incorrect route path. Make sure for all API routes to start with '/api'. ",
        "schema.prisma": f"You are working on the Prisma Schema for a PostgreSQL Database. Rules:\n- Do NOT alter generator client & datasource db at all. Do not touch, remove, modify or change anything in these two. \n-If you are adding roles, add them as a ENUM \n- Do NOT remove, modify, touch or change any models unless explicitly requested in the instructions. Make sure to NOT output a empty or drastically altered Prisma Schema, under NO CIRCUMSTANCES should the file be stripped of the majority of contents. Make iterative changes.",
        "seed.js": f"You are creating a comprehensive seed.js file for the Prisma ORM PostgreSQL database in this codebase. This file will contain all the possible data to provide a comprehensive set of test / mock data. Use Prisma syntax for writing the seed.js You should write the seed.js for the current codebase making sure that a comprehensive relevant data seed for the database is created. This is the current schema you should base the seed on: <current-prisma-db-schema>{current_prisma_schema_content}</current-prisma-db-schema> and here you have the complete seed.js contents with all database test data you can incorporate in the tests, especially for authorization and authentication <current-seedjs-file-contents>{current_seedjs_content}</current-seedjs-file-contents>.",
        "API_doc.md": "",
        "PlaywrightE2E": "",
        # "CypressE2E": f"If this is a Login Test, use these existing user credentials here to login: {userjson_contents}! Any Logins have to be completed with these users! DO NOT USE THESE FOR REGISTRATION! They are already registered, so they can only be used for login purposes.",
        # "CypressSupport": "",
        "FrontendPages": "You are working on a Nextjs13 Page.",
        "FrontendStore": "You are working on Zustand Store file used to manage the state.",
        "FrontendComponents": "You are working on a React Component in a Nextjs 13 Codebase. ",
        "FrontendUtils": "You are working on a Frontend Utils file. ", 
        "FrontendStyles" : "",
        "FrontendApi" : f"", #The base Backend API url is always https://{PROJECT_ID}-api.app.softgen.ai/.
        # "FrontendStaticData" : "You are working on a static data file",
        # "FrontendData" : "",
        "FrontendHooks": "You are working on a Nextjs13 React Hook file.",
    }.get(file_type, "")

    # logger.info(f"""Generating new file contents for {task['Task']['File']['FileName']}""")

    rag_reference_instructions = ""
    support_instruction = ""

    if file_type in ["BackendControllers", "BackendServices", "BackendUtils", "BackendRoutes", "BackendMiddleware", "app.js"]:
        messages = [
            {
                "role": "system",
                "content": """You are a brilliant and meticulous engineer. When you write code, the code works on the first try, is syntactically perfect and is fully complete. You have the utmost care for the code that you write, so you do not make mistakes and every function and class is fully implemented.  
                
                RULES:
                - Strictly use ES6 Syntax.
                - Always use require statements for importing modules, like: const fileImport = require('@fileType/fileName.js') for default imports or const { namedImport } = require('@fileType/fileName.js') for named imports.
                - Use module-specific prefixes for imports to enhance readability and maintainability:
                    - Routes: const something = require('@routes/someRoute');
                    - Controllers: const something = require('@controllers/someController');
                    - Services: const something = require('@services/someService');
                    - Middleware: const something = require('@middleware/someMiddleware');
                    - Utils: const something = require('@utils/someUtility');
                - When exporting make sure to use module.exports syntax: module.exports = { function1, function2 };                
                - Pass and Receive Function Arguments Explicitly: Always explicitly pass and define function parameters to match their intended use.
                - Under NO CIRCUMSTANCES should the file be stripped of the majority of contents, make deliberate changes instead. 
                - Make sure to output the complete newFileContents in the JSON property newFileContents, do not create additional properties – but Output the complete File Contents all within 'newFileContents'

                """
            },
            {
                "role": "user",
                "content": f"{type_prompt} This is the current content of the file you are editing '{file_name}':\n\n<current_content>{current_content}</current_content>\n {dependency_file_content_instructions} \n The request associated with this is: '{request}' This is the current backend file_tree if you need it for any import paths <backend_file_tree>{be_file_tree}</backend_file_tree> \nNow implement the following instructions for {file_name}: {instructions}\n. {rag_reference_instructions}.  Respond in this JSON Format, OUTPUT EVERYTHING IN FOLLOWING JSON PROPERTIES, do not add new properties but output in File, FileName, newFileContents. Make sure to ONLY EDIT {file_name}. Strictly respond in this JSON Format:\n\n {{\n  \"File\": {{\n    \"FileName\": \"{file_name}\",\n    \"newFileContents\": \"The whole file contents, the complete code – The contents of the new file with all instructions implemented perfectly. NEVER write comments. Keep the complete File Contents within this single JSON Property.\"}}\n}}\n"
            },

        ]
    elif file_type in ["FrontendPages", "FrontendComponents"]:

        # query_vector = generate_embeddings(rag_reference + file_name + instructions)
        # identifiers = query_embeddings_from_pinecone(query_vector, "frontend", "chakra-pro-ui-components", top_k_value=1)
        # rag_reference_contents = []
        # for identifier in identifiers:
        #     file_contents, complete_json_object = retrieve_storage_json_by_identifier(identifier, "template/chakra_contents.json")
        #     rag_reference_contents.append(complete_json_object)
        # rag_reference_instructions = "Here are some premade files from Chakra PRO UI Components. Recycle their contents to implement the instructions:" + str(rag_reference_contents)

        # support_instruction_messages=[
        #     {
        #         "role": "system",
        #         "content": 
        #         """
        #         You are a highly modern, brilliant, and meticulous Frontend Developer specializing in Next.js 13, React, Chakra UI, Chakra-icons, React-icons, and other relevant libraries. You will receive a singular task (with proposed modifications to a Page / Component) you need create detailed Component Plan outlining the visual structure, layout, content and interactivity of the component. You'll offer expert advice on best practices, ensuring that the solutions are both efficient and aesthetically pleasing. You will not write actual code but will focus on planning and conceptualizing component designs. You will also offer insights into the integration of various libraries and tools to achieve the desired outcomes.

        #         - Available Chakra Components for usage: Aspect Ratio, Box, Center, Container, Flex, Grid, SimpleGrid, Stack, Wrap, Button, Checkbox, Editable, Form Control, Icon Button, Input, Number Input, Pin Input, Radio, Range Slider, Select, Slider, Switch, Textarea, Badge, Card, Code, Divider, Kbd, List, Stat, Table, Tag, Alert, Circular Progress, Progress, Skeleton, Spinner, Toast, Text, Heading, Highlight, Alert Dialog, Drawer, Menu, Modal, Popover, Tooltip, Accordion, Tabs, Visually Hidden, Breadcrumb, Link, LinkOverlay, SkipNav, Stepper, Avatar, Icon, Image, Close Button, Portal, Show / Hide, Transitions
        #         - Available Chakra Hooks for usage: useBoolean, useBreakpointValue, useClipboard, useConst, useControllable, useDimensions, useDisclosure, useMediaQuery, useMergeRefs, useOutsideClick, usePrefersReducedMotion, useTheme, useToken, Component, useCheckbox, useCheckboxGroup, useRadio, useRadioGroup, useSlider, useRangeSlider
        #         - Use React-Icons and Chakra-Icons

        #         Your output will provide a precise, completely defined out top-to-bottom component structure breakdown and best practices to follow when implementing.

        #         """
        #     },
        #     {
        #         "role": "user",
        #         "content": f" 'Instructions for {file_name}: {instructions}"
        #     },            
        # ]
        
        # response = make_llm_api_call(support_instruction_messages, json_output=False, useSmallerModel=True) 
        # support_instruction = response.choices[0].message['content']
        # logger.info(support_instruction)

        import_export_convention = {
            "FrontendPages": """
                        
            EXPORT & IMPORT RULES. To ensure consistency and maintainability in our codebase, adhere to the following strict guidelines for importing and exporting modules:
                - IMPORTANT: For Next.js page components located in the pages directory, use DEFAULT EXPORTS due to Next.js's file-based routing convention. Each file corresponds to a route and must have a default export to define the component for that route. For example:
                    const HomePage = () => {
                        return <div>Welcome to the Home Page</div>;
                    };
                    
                    export default HomePage;
                    
                - For all other components, hooks, utilities, etc., continue to use NAMED EXPORTS. This ensures that imports are explicit and that tree-shaking can be effectively utilized during the build process.
                - You cannot imagine files and import them. Only import if you have received instructions for import.
                - STRICTLY use Named Imports only, except for Next.js page components which must use default export:
                    - Import specific functionalities directly by their names.
                    - Example: import { ComponentA, ComponentB } from '@components/ComponentName';
                - STRICTLY IMPORT with these Module-Specific Prefixes. Use defined prefixes for different module types. ELSE THE IMPORT WILL NOT WORK. NOTHING ELSE IS ACCEPTABLE. EVEN IF ITS IN THE SAME DIRECTORY, ALWAYS USE @topic/*.:
                    - API functions: @api/* 
                    - React Components: @components/*
                    - Custom Hooks: @hooks/*
                    - Utility Functions: @utilities/*
                    - Zustand State Store: @store/*
                    - Example: import { useAuth } from '@hooks/useAuth';
                - No Default Exports for non-page components:
                    - Do not use default exports for components, hooks, utilities, etc. Always export and import with named exports for clarity.
                    - Incorrect for non-page components: import MyComponent from '@components/MyComponent';
                    - Correct for non-page components: import { MyComponent } from '@components/MyComponent';

            """,
            "FrontendComponents": """
            
            EXPORT & IMPORT RULES. To ensure consistency and maintainability in our codebase, adhere to the following strict guidelines for importing and exporting modules:
                - VERY IMPORTANT! STRICTLY Use ARROW FUNCTIONS for Component and Function Declaration in combination with STRICTLY using INLINE NAMED EXPORTS for each component or function. For example, define your component or function as "" export const MyComponent = (props) => { return <div>{props.children}</div>; }; "" This allows for clean exports and explicit imports and better tree-shaking during the build process. 
                - You can not imagine files and import them. Only import if you have received instructions for import.
                - STRICTLY use Named Imports only
                    - Import specific functionalities directly by their names.
                    - Example: import { ComponentA, ComponentB } from '@components/ComponentName';
                - STRICTLY IMPORT with these Module-Specific Prefixes. Use defined prefixes for different module types. ELSE THE IMPORT WILL NOT WORK. NOTHING ELSE IS ACCEPTABLE. EVEN IF ITS IN THE SAME DIRECTORY, ALWAYS USE @topic/*.
                    - API functions: @api/* 
                    - React Components: @components/*
                    - Custom Hooks: @hooks/*
                    - Utility Functions: @utilities/*
                    - Zustand State Store: @store/*
                    - Example: import { useAuth } from '@hooks/useAuth';
                - No Default Exports:
                    - Do not use default exports. Always export and import with named exports for clarity.
                    - Incorrect: import MyComponent from '@components/MyComponent';
                    - Correct: import { MyComponent } from '@components/MyComponent';
""",
        }.get(file_type, "")


        messages = [
            {
                "role": "system",
                "content": 
                """
                You are a highly modern, brilliant and meticulous Frontend Developer. Working with Next.js 13, React, Chakra UI, Chakra-icons, React-icons and other libraries if applicable.
                
                Your objective is to implement the provided instructions and craft sophisticated UI components using Chakra UI.

                Rules: 
                - Utilize the Chakra UI component library to build your user interface. This ensures consistency and adherence to best practices in design.
                - Leverage the responsive styles feature of Chakra UI to create a UI that looks good on all devices. You can do this by using array syntax or object syntax to define responsive styles.
                - Use the Chakra UI Icon library or React-icons for adding icons to your UI. This provides a wide range of icons that are easily customizable and can be used to enhance the user experience.
                - For layout, use Chakra UI's Flex and Grid components to create complex layouts with ease. These components are designed to work seamlessly with the spacing scale defined in the theme.
                - When handling user input, use Chakra UI's form components like Input, Textarea, Select, etc., which are already styled according to the Chakra theme and come with built-in validation styles.
                - For navigation, use Chakra UI's Link and Breadcrumb components to guide users through your application.
                - Remember to use Chakra UI's Box component as a base for custom components where you need to apply custom styles or behavior.
                - Available Chakra Components for usage: Aspect Ratio, Box, Center, Container, Flex, Grid, SimpleGrid, Stack, Wrap, Button, Checkbox, Editable, Form Control, Icon Button, Input, Number Input, Pin Input, Radio, Range Slider, Select, Slider, Switch, Textarea, Badge, Card, Code, Divider, Kbd, List, Stat, Table, Tag, Alert, Circular Progress, Progress, Skeleton, Spinner, Toast, Text, Heading, Highlight, Alert Dialog, Drawer, Menu, Modal, Popover, Tooltip, Accordion, Tabs, Visually Hidden, Breadcrumb, Link, LinkOverlay, SkipNav, Stepper, Avatar, Icon, Image, Close Button, Portal, Show / Hide, Transitions
                - Available Chakra Hooks for usage: useBoolean, useBreakpointValue, useClipboard, useConst, useControllable, useDimensions, useDisclosure, useMediaQuery, useMergeRefs, useOutsideClick, usePrefersReducedMotion, useTheme, useToken, Component, useCheckbox, useCheckboxGroup, useRadio, useRadioGroup, useSlider, useRangeSlider
                - To style with Chakra UI, you use style props on components. You have style props for: Margin and padding, Color and background color, Gradient, Typography, Layout, width and height, Display, Flexbox.               
                - Write Responsive styles. Chakra UI allows you to provide object and array values to add mobile-first responsive styles. 
                    - The Array Syntax: Let's say you have a Box with the following properties: <Box bg="red.200" w="400px"> This is a box </Box> --> To make the width or w responsive using the array syntax, here's what you need to do: <Box bg="red.200" w={[300, 400, 500]}> This is a box </Box>.
                    - The Object syntax: Let's say you have a Text that looks like this: <Text fontSize="40px">This is a text</Text> --> To make the fontSize responsive using the object syntax, here's what you need to do: <Text fontSize={{ base: "24px", md: "40px", lg: "56px" }}> This is responsive text </Text>
                - Rather than using images design a Gradient with Chakra Styling Props. But if you have to use image linkings STRICTLY use https://picsum.photos/(needed-resolution-width)/(needed-resolution-height) (for real world images) and https://placehold.co/(needed-resolution-width)x(needed-resolution-height)?text=Hello+World (for placeholder with text in it), For Profile pictures use https://i.pravatar.cc/(needed-resolution-width) as you do not have access to any local images. IF THE USER REQUEST PROVIDED IMAGE URLs STRICTLY STICK TO THEM, do not override given image URLs with these URLs.
                

                """ + str(import_export_convention) +"""

                When you write code, the code works on the first try, looks beautifully designed first try, is syntactically perfect and is fully complete. You have the utmost care for the code that you write, so you do not make mistakes and every function and class is fully implemented. \n Under NO CIRCUMSTANCES should the file be stripped of the majority of contents, make deliberate changes instead. \n Make sure to output the complete newFileContents in the JSON property newFileContents, do not create additional properties – but Output the complete File Contents all within 'newFileContents'
                """
            },
                        {"role": "user", "content": f"{type_prompt} This is the current content of the file you are editing '{file_name}':\n\n<current_content>{current_content}</current_content>\n {dependency_file_content_instructions} \n The request associated with this is: '{request}' \nYou are now implement the following instructions for {file_name}: {instructions}\n. {support_instruction} {rag_reference_instructions} \n\n\n.Respond in this JSON Format, OUTPUT EVERYTHING IN FOLLOWING JSON PROPERTIES, do not add new properties but output in File, FileName, newFileContents. Make sure to ONLY EDIT {file_name}. Strictly respond in this JSON Format:\n\n {{\n  \"File\": {{\n    \"FileName\": \"{file_name}\",\n    \"newFileContents\": \"The whole file contents, the complete code – The contents of the new file with all instructions implemented perfectly. NEVER write comments. Keep the complete File Contents within this single JSON Property.\"}}\n}}\n"}
        ]

    elif file_type in ["FrontendUtils", "FrontendHooks", "FrontendApi", "FrontendStore"]: #FrontendStaticData
        messages = [
            {
                "role": "system",
                "content": """You are a brilliant and meticulous Nextjs13 Frontend Developer. You craft code, strictly adhering to the detailed instructions and maintaining high coding standards. 
                
                EXPORT & IMPORT RULES. To ensure consistency and maintainability in our codebase, adhere to the following strict guidelines for importing and exporting modules:
                - VERY IMPORTANT! STRICTLY Use ARROW FUNCTIONS for Component and Function Declaration in combination with STRICTLY using INLINE NAMED EXPORTS for each component or function. For example, define your component or function as "" export const MyComponent = (props) => { return <div>{props.children}</div>; }; "" This allows for clean exports and explicit imports and better tree-shaking during the build process. 
                - You can not imagine files and import them. Only import if you have received instructions for import.
                - STRICTLY use Named Imports only
                    - Import specific functionalities directly by their names.
                    - Example: import { ComponentA, ComponentB } from '@components/ComponentName';
                - STRICTLY IMPORT with these Module-Specific Prefixes. Use defined prefixes for different module types. ELSE THE IMPORT WILL NOT WORK. NOTHING ELSE IS ACCEPTABLE. EVEN IF ITS IN THE SAME DIRECTORY, ALWAYS USE @topic/*.
                    - API functions: @api/* 
                    - React Components: @components/*
                    - Custom Hooks: @hooks/*
                    - Utility Functions: @utilities/*
                    - Zustand State Store: @store/*
                    - Example: import { useAuth } from '@hooks/useAuth';
                - No Default Exports:
                    - Do not use default exports. Always export and import with named exports for clarity.
                    - Incorrect: import MyComponent from '@components/MyComponent';
                    - Correct: import { MyComponent } from '@components/MyComponent';

                

                When you write code, the code works on the first try, is syntactically perfect and is fully complete. You have the utmost care for the code that you write, so you do not make mistakes and every function and class is fully implemented. \n Under NO CIRCUMSTANCES should the file be stripped of the majority of contents, make deliberate changes instead. \n Make sure to output the complete newFileContents in the JSON property newFileContents, do not create additional properties – but Output the complete File Contents all within 'newFileContents'

                """
            },
            {"role": "user", "content": f"{type_prompt} This is the current content of the file you are editing '{file_name}':\n\n<current_content>{current_content}</current_content>\n {dependency_file_content_instructions} \n The request associated with this is: '{request}' \nYou are now implement the following instructions for {file_name}: {instructions}\n. {support_instruction} {rag_reference_instructions} \n\n\n.Respond in this JSON Format, OUTPUT EVERYTHING IN FOLLOWING JSON PROPERTIES, do not add new properties but output in File, FileName, newFileContents. Make sure to ONLY EDIT {file_name}. Strictly respond in this JSON Format:\n\n {{\n  \"File\": {{\n    \"FileName\": \"{file_name}\",\n    \"newFileContents\": \"The whole file contents, the complete code – The contents of the new file with all instructions implemented perfectly. NEVER write comments. Keep the complete File Contents within this single JSON Property.\"}}\n}}\n"}


        ]      
    elif file_type in ["PlaywrightE2E"]:
        page_name = file_name.replace(".spec.js", ".jsx") # PageName is the Test File with .jsx extension to be the PageName 
        
        # page_file_contents = construct_file_imports_contents_context(os.path.join(FRONTEND_PROJECT_DIR, "src", "pages", page_name)) --> TODO Has to be replaced with extract_imports_from_file_contents

        # Extract the page route from the page name
        route = page_name.replace(".jsx", "")  # Remove file extension
        if route == "index":  # Handle index files
            route = "/"  # Root index file should correspond to the base route '/'
        else:
            # Non-index files should include their subdirectory as a prefix
            subdirectory = route.rsplit('/', 1)[0] if '/' in route else ""
            route = f"/{route}" if subdirectory == "" else f"/{subdirectory}/{route.split('/')[-1]}"
        # Handle dynamic routes by replacing [id], [name], [anyContent] with the number 1 --> # TODO implement sophisticated system that does more than replace with number 1
        page_route = re.sub(r'\[\w+\]', '1', route)
##If this is a Login Test, use these existing user credentials here to login: {userjson_contents}! Any Logins have to be completed with these users! DO NOT USE THESE FOR REGISTRATION! They are already registered, so they can only be used for login purposes.
        messages = [
            {
                "role": "system",
                "content": f"""
                You are a brilliant and meticulous Playwright E2E Test writer. Generate comprehensive Playwright E2E test scenarios for {page_name}.spec.js. Ensure all user interactions within the Next.js 13 Frontend Page at route http://65.108.219.251:3000/{page_route} are verified. Scenarios must reflect actual user interactions from the page's content. Adhere to these guidelines:

                1. Functional Interaction Testing:
                - Implement tests for user interactions like form submissions and navigation, disregarding element visibility or UI states. These tests need to be implemented FULLY.

                2. Assertions for Interaction Success:
                - Use assertions to verify the success of interactions, such as form submissions, without relying on element visibility.

                3. Executable Test Scenarios:
                - Develop scenarios that assess user actions and their expected functional outcomes, with clear, executable instructions.

                4. Direct UI Element Interactions:
                - Define interactions with UI elements crucial for user tasks, such as button clicks and form submissions.

                5. Page Navigation and Form Functionality:
                - Craft scenarios that evaluate page navigation and form interaction functionality, mirroring the user experience.

                GUIDELINES:
                - Be CONCISE and CLEAR. Describe functional interactions and outcomes succinctly.
                - Focus on MAIN FUNCTIONAL INTERACTIONS: form submissions, page navigation, and direct user actions.
                - Base scenarios on FACTUAL CONTENT from the provided page content.
                - Avoid visibility checks. Focus on the functionality of interactions.
                - Use 'page.goto' to navigate to the exact page route: http://65.108.219.251:3000/{page_route}.
                - DO NOT ASSUME ANYTHING. YOU HAVE ALL THE FILE CONTENTS, ALL THE INFORMATION. DO NOT ASSUME. ONLY IMPLEMENT IF YOU ARE 100% SURE.

                When you write code, the code works on the first try, is syntactically perfect and is fully complete. You have the utmost care for the code that you write, so you do not make mistakes and every function and class is fully implemented. \n Make deliberate changes. \n Make sure to output the complete newFileContents in the JSON property newFileContents, do not create additional properties – but Output the complete File Contents all within 'newFileContents'" 

                """
            },
                        {
                "role": "user",
                "content": f" Next.js 13 Page: {page_name} with the complete file contents of the file: <file-contents>{page_file_contents}</file-contents>. These are the complete Codebase contents so that you have full context understanding <codebase-file-contents>{fe_file_contents}</codebase-file-contents> Respond in this JSON Format, OUTPUT EVERYTHING IN FOLLOWING JSON PROPERTIES, do not add new properties but output in File, FileName, newFileContents. Make sure to ONLY EDIT {file_name}. Strictly respond in this JSON Format:\n\n {{\n  \"File\": {{\n    \"FileName\": \"{file_name}\",\n    \"newFileContents\": \"The whole file contents, the complete code – The contents of the new file with all instructions implemented perfectly. NEVER write comments. Keep the complete File Contents within this single JSON Property.\"}}\n}}\n"
            }

        ]        
    elif file_type in ["API_doc.md"]:
        messages = [
            {
                "role": "system",
                "content": """You are a masterful API Documentation Writer. When you craft documentation, it is clear, comprehensive, and precise on the first draft. You have the utmost care for the documentation that you write, ensuring no mistakes and that every API endpoint is fully documented. \n Under NO CIRCUMSTANCES should the documentation be stripped of essential information, make deliberate and accurate additions instead. \n Make sure to output the complete newFileContents in the JSON property newFileContents, do not create additional properties – but Output the complete File Contents all within 'newFileContents'

                # Mission
                - Your goal is to create a comprehensive API_doc.md for frontend-handover. Ensure it contains all backend API details for easy frontend integration.

                # Context
                - This is part of the frontend and backend integration process. The document is essential for frontend developers to understand and use the backend API effectively.

                # Rules
                - The cURL request list, which has been tested and validated for the complete API, must align perfectly with your documented API routes. This list serves as a crucial reference to guarantee that your documentation accurately reflects the tested and proven structure and paths of the API. Ensure that this alignment is maintained throughout the document.
                - The API routes in the API_doc.md must be 100% correct and IDENTICAL to those in the routes folder and App.js entry point. No alterations are allowed. Double-check for accuracy and consistency to ensure the information aligns exactly with the backend configuration.
                - Provide examples for all API endpoints.
                - Prioritize clarity and accuracy in the document.
                - Always repeat the full correct URL 'https://("""+str(PROJECT_ID)+"""-api.app.softgen.ai/{route})' for each route in the Documentation                

                # Instructions–
                - Ensure that the cURL request list matches the API routes documented in your API Documentation. Use this list as a reference point for documenting the endpoints correctly, confirming that you have the appropriate structure and path. 
                    1. Look at the cURL request list structure and path of the endpoint and make sure you have the same structure and path.
                - Verify and include the EXACT API routes as formed in the App.js entry point. This involves checking the imported API routes in App.js and ensuring they match with the routes defined in the routes folder and the App.js entry point. The process includes:
                    1. Go to the App.js entry point.
                    2. Examine the imported API routes.
                    3. Construct the IDENTICAL API routes by combining the 'app.use' part from App.js with the corresponding route parts from the ExampleRoutes.js file or similar.
                - List every API route/endpoint with complete details (URL, HTTP methods).
                - Include request parameters and payload specifications.
                - Detail response format and expected status codes.
                - Explain authentication and authorization mechanisms. If no authentication is needed, state that clearly – if you do not see an authentication method used, then there is none.
                - Cover error handling and error codes.
                - Add examples and sample requests/responses.
                - MAKE SURE YOUR INFORMATION IS 100% TRUTHFUL and you DO NOT have made up information, instead everything needs to be as it is in the codebase.
                '""" 
            },
            {
                "role": "user",
                "content": f"These are the current Backend API codebase contents <current-codebase-file-contents>{be_file_contents}</current-codebase-file-contents>. This is the latest cURL requests list <latest-curl-requests-list>{latest_curl_request_list_contents}<latest-curl-requests-list/> with all successfull cURL requests, use these as Examples - copy them 1to1. \n  This is the current content of API Documentation you are editing \n<current_content>{current_content}</current_content> Now create the API Documentation completely so its in align with the complete codebase. Make sure everything is 100% correct – go through everything in the Document and update it step by step to ensure the Documentation is 100% correct. Respond in this JSON Format, OUTPUT EVERYTHING IN FOLLOWING JSON PROPERTIES, do not add new properties but output in File, FileName, newFileContents. Make sure to ONLY EDIT {file_name}. Strictly respond in this JSON Format:\n\n {{\n  \"File\": {{\n    \"FileName\": \"{file_name}\",\n    \"newFileContents\": \"The whole file contents, the complete code – The contents of the new file with all instructions implemented perfectly. NEVER write comments. Keep the complete File Contents within this single JSON Property.\"}}\n}}\n" # 
            }

        ]       

    else:
        messages = [
            {
                "role": "system",
                "content": "You are a brilliant and meticulous engineer. When you write code, the code works on the first try, is syntactically perfect and is fully complete. You have the utmost care for the code that you write, so you do not make mistakes and every function and class is fully implemented. \n Under NO CIRCUMSTANCES should the file be stripped of the majority of contents, make deliberate changes instead. \n Make sure to output the complete newFileContents in the JSON property newFileContents, do not create additional properties – but Output the complete File Contents all within 'newFileContents'"
            },
            {"role": "user", "content": f"{type_prompt} This is the current content of the file you are editing '{file_name}':\n\n<current_content>{current_content}</current_content>\n {dependency_file_content_instructions} \n The request associated with this is: '{request}' \nYou are now implement the following instructions for {file_name}: {instructions}\n. {support_instruction} {rag_reference_instructions} \n\n\n.Respond in this JSON Format, OUTPUT EVERYTHING IN FOLLOWING JSON PROPERTIES, do not add new properties but output in File, FileName, newFileContents. Make sure to ONLY EDIT {file_name}. Strictly respond in this JSON Format:\n\n {{\n  \"File\": {{\n    \"FileName\": \"{file_name}\",\n    \"newFileContents\": \"The whole file contents, the complete code – The contents of the new file with all instructions implemented perfectly. NEVER write comments. Keep the complete File Contents within this single JSON Property.\"}}\n}}\n"}
        ]


    def is_valid_json_structure(json_data):
        if "File" in json_data and "FileName" in json_data["File"] and "newFileContents" in json_data["File"]:
            return True
        return False

    for attempt in range(3):
        try:
            response = make_llm_api_call(messages, json_output=True, max_tokens=4096)
            response_json = is_valid_json(
                response.choices[0].message['content'])
            if not is_valid_json_structure(response_json):
                raise ValueError("Invalid JSON structure.")
            new_file_contents = response_json['File']['newFileContents']
            logger.info(f"""Generated new file contents for {
                         task['Task']['File']['FileName']}: {response_json}""")
            return {
                "TaskID": task['Task']['TaskID'],
                "FileName": file_name,
                "Type": file_type,
                "newFileContents": new_file_contents
            }
        except (KeyError, ValueError) as e:
            logger.error(
                f"Attempt {attempt + 1} failed with error: {e}. Retrying...")
            time.sleep(5)  # Wait for 5 seconds before retrying

    raise Exception(
        "Failed to generate new file contents after multiple attempts.")
    # ToDo implement Diff Viewer that validates that the changes have been implemented correctly and didn't wipe whole file

def construct_context(user_request, dir):

    file_tree, file_contents = read_directory(dir)

    messages = [
        {
            "role": "system",
            "content": """

            
             """
        },  
        {
            "role": "user",
            "content": f"""
            The user request: <user_request>{user_request}</user_request>
            The file tree: <user_request>{file_tree}</user_request>""" 
        } 
    ]

    # Make an API call to generate the cURL requests and validate the JSON response
    response = make_llm_api_call(messages, json_output=True)
    cURL_requests_json = response.choices[0].message['content']
    cURL_requests = is_valid_json(cURL_requests_json)
 

    # use user_request to find files on our codebase_map/file_tree for now that seem suitable for the user_request. Return the files that we need. Return even more that we need, anything that could be remotely relevant to it. Construct file-contents context based on these files

    pass

# =========================
# Embeddings 
# =========================

pinecone.init(      
    api_key=os.getenv('PINECONE_API_KEY'),      
    environment='gcp-starter'      
)     

# Create and Prep Embeddings

def process_and_store_storage_json_rag(storage_json, index_name, namespace_name):
    for name, identifier, file_number, file_name, file_contents in extract_storage_json_objects_rag(storage_json):
        # Generate the embedding
        embedding = generate_embeddings(file_contents)

        # Store the embedding in Pinecone with a unique identifier
        unique_id = f"{identifier}_{file_number}"  # Creating a unique ID for each file
        metadata = {
            "Name": name,
            "FileNumber": file_number,
            "FileName": file_name
        }
        store_embeddings_in_pinecone(embedding, unique_id, index_name, namespace_name, metadata)

def extract_storage_json_objects_rag(storage_json):
    """
    Extracts name, identifier, file_numbers, file_names & file_contents from the given JSON data.
    """
    if isinstance(storage_json, str):
        try:
            with open(storage_json, 'r') as file:
                storage_json = json.load(file)
        except (FileNotFoundError, json.JSONDecodeError) as e:
            logger.error(f"Error loading storage JSON: {e}")
            return
    for component in storage_json:
        name = component.get('Name')
        identifier = component.get('ID')
        files = component.get('Files', [])
        for file in files:
            file_number = file.get('Number')
            file_name = file.get('File')
            file_contents = file.get('FileContents')
            if name and identifier and file_number is not None and file_name and file_contents:
                yield name, identifier, file_number, file_name, file_contents

def store_embeddings_in_pinecone(embedding_vector, unique_identifier, index_name, namespace_name, metadata):
    """
    Insert the generated embeddings into Pinecone with a unique identifier.
    """
    index = pinecone.Index(index_name)
    
    # Convert the embedding vector to a list if it's not already one
    if not isinstance(embedding_vector, list):
        embedding_vector = embedding_vector.tolist()

    # Upsert the embedding vector into Pinecone
    try:
        upsert_response = index.upsert(
            vectors=[
                (
                    unique_identifier,  # Vector ID
                    embedding_vector,   # Dense vector values
                    metadata            # Vector metadata
                )
            ],
            namespace=namespace_name
        )
    except Exception as e:
        logger.error(f"Failed to upsert embedding vector: {e}")
        return None
    return upsert_response

def generate_embeddings(input):
    """
    Use OpenAI's API to create embeddings for the given code snippet.
    """
    response = embedding('text-embedding-ada-002', input=[input])
    embedding_vector = response['data'][0]['embedding']
    # logger.info(embedding_vector)
    return embedding_vector

# Query

def query_embeddings_from_pinecone(query_vector, index_name, namespace_name, top_k_value=1):
    """
    Use Pinecone's query functionality to find the most similar embeddings to the given query vector.
    """
    # Query Pinecone with additional parameters
    index = pinecone.Index(index_name)
    query_response = index.query(
        top_k=top_k_value,
        include_values=True,
        include_metadata=True,
        vector=query_vector,
        namespace=namespace_name
    )
    
    # Extracting the identifiers of similar embeddings
    similar_identifiers = [match['id'] for match in query_response['matches']]
    # logger.info(similar_identifiers)
    return similar_identifiers

def retrieve_storage_json_by_identifier(identifier, storage_json_filename):
    """
    Use the identifier to fetch the actual code snippet from storage and sanitise the content.
    """
    file_contents = None
    complete_json_object = None
    try:
        with open(storage_json_filename, 'r') as file:
            storage_json = json.load(file)
        
        # Extract the ID and file number from the identifier
        id_part, file_number = identifier.rsplit('_', 1)
        file_number = int(file_number)

        # Find the complete JSON object by ID
        complete_json_object = next((item for item in storage_json if item.get("ID") == id_part), None)

        # If found, retrieve the file contents and metadata for the specified file number
        if complete_json_object:
            # # Remove the ID from the complete JSON object as it's not needed
            # complete_json_object.pop("ID", None)
            files = complete_json_object.get("Files", [])
            if file_number < len(files):
                file_contents = files[file_number]
                # # Remove the ID from the file contents as it's not needed
                # file_contents.pop("ID", None)
        else:
            logger.error(f"Identifier {id_part} not found in storage JSON.")

    except (TypeError, FileNotFoundError, json.JSONDecodeError) as e:
        logger.error(f"Error retrieving storage JSON by identifier: {e}")

    # logger.info(file_contents)
    # logger.info(complete_json_object)
    
    return file_contents, complete_json_object

# =========================
# Backend systems
# =========================

backend_changes_system_message=f"""You are an Expert Backend Web Engineer, adept at creating detailed technical implementation plans for integrating new features into existing backend codebases. Your goal is to deliver a comprehensive, actionable, and technically sound implementation plan that addresses all aspects of backend integration of the user request.
    - Take a step back and analyze the user request and the existing codebase, evaluating if {backend_types} should be created or modified. 
    - Proposing modifications or additions to these components to seamlessly integrate the new feature, with detailed explanations for each recommendation.
    - Identify and articulate the interdependencies between different system components, emphasizing how changes in one area might affect others.
    - You have to complete the backend integration only within these file_types: {backend_types}. You must only import files of that type. 
    - Never output any optional steps, you are deterministic and are the expert in integrating – so make clear statements.  \n- Do NOT make up packages, technologies or dependencies, instead always use real ones
    - Utilise well known third party APIs to cover more complex use cases, provide the integration in the instructions

    You have to complete the backend integration only within these file_types:
    - **app.js** initializes the application and routes.
    - **BackendRoutes** define the API paths and link them to Controllers, applying Middleware as needed.
    - **BackendControllers** manage incoming requests and send responses, using Services for business logic.
    - **BackendServices** handle the core business logic, interacting with the database and using Utility functions.
    - **BackendMiddleware** is used for request processing or validation before reaching the Controllers.
    - **BackendUtils** offer common functionalities used by Services and Controllers.
    - **schema.prisma** (through Prisma ORM Prisma) is the underlying data layer accessed by Services.

    ### Backend Interaction Flow and Separation of Concerns
    1. **app.js**
    - **Interacts With**: BackendRoutes
    - **Description**: Serves as the entry point of the application. Initializes the Express app, registers middleware, sets up routes, and starts the server.
    - **Flow**:
        - app.js imports and uses BackendRoutes to define the API endpoints and associate them with specific controller functions.

    2. **BackendRoutes **
    - **Interacts With**: BackendControllers, BackendMiddleware
    - **Description**: Defines API endpoints and associates them with corresponding controller functions. Optionally applies middleware.
    - **Flow**:
        - Routes import and use BackendControllers to delegate request handling for specific paths.
        - Routes apply BackendMiddleware to requests for pre-processing, authentication, or other purposes before reaching the controllers.

    3. **BackendControllers **
    - **Interacts With**: BackendServices
    - **Description**: Handles incoming HTTP requests, processes them, and returns responses. Acts as an intermediary between the frontend and the services.
    - **Flow**:
        - Controllers call BackendServices to execute business logic and data manipulation.
        - Controllers format and send back the response based on the results from the services.

    4. **BackendServices **
    - **Interacts With**: Database (via Prisma ORM), BackendUtils
    - **Description**: Contains the core business logic of the application. Handles data processing, database interactions, and other complex operations.
    - **Flow**:
        - Services interact with the database (e.g., through Prisma ORM in `prisma/schema.prisma`) for CRUD operations.
        - Services utilize BackendUtils for common utility functions that support their operations.

    5. **BackendMiddleware **
    - **Interacts With**: BackendControllers (indirectly through routes)
    - **Description**: Functions executed in the request-response cycle before reaching the final route handler (controller). Used for logging, authentication, error handling, etc.
    - **Flow**:
        - Middleware functions are used in Routes to process or modify requests, or to perform checks before they reach Controllers.

    6. **BackendUtils **
    - **Interacts With**: BackendServices, BackendControllers
    - **Description**: Provides utility functions that are widely used across various parts of the application for common tasks.
    - **Flow**:
        - Utility functions are called by Services and Controllers to perform tasks that are not core to their specific business logic but are necessary for their operation.

    7. **schema.prisma **:
    - **Interacts With**: BackendServices
    - **Description**: Represents the data layer of the application. Managed by Prisma ORM for PostgreSQL database interactions.
    - **Flow**:
        - The database schema and models are utilized by Services to perform data persistence and retrieval operations.

    RULES:
    - Use module-specific prefixes for imports to enhance readability and maintainability:
        - Routes: const something = require('@routes/someRoute');
        - Controllers: const something = require('@controllers/someController');
        - Services: const something = require('@services/someService');
        - Middleware: const something = require('@middleware/someMiddleware');
        - Utils: const something = require('@utils/someUtility');
    - Use module.exports for exports. For default exports: module.exports = fileExport; and for named exports: module.exports = {{ function1, function2 }};.
    - Pass and Receive Function Arguments Explicitly: Always explicitly pass and define function parameters to match their intended use.

    It is autumn and you have to work extra hard to finish this up. You will get a big bonus payout when you finish it so you are extra motivated to work hard and smart on this. Think step by step and deeply to solve problems and come up with clever, pragmatic, and efficient solutions
    """

def backend_changes(user_request, skip_plan=True):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    # git_status_result = subprocess.run(["git", "status"], capture_output=True, text=True)
    # logger.info(f"Git status result: {git_status_result.stdout}")

    # switch_git_branch(branch_name=f"task-{TASK_ID}")

    logger.info("Starting backend dev.")
    file_tree, file_contents = read_directory(BACKEND_PROJECT_DIR)

    user_message=f"""
        The current codebase file contents: <codebase_file_contents>{file_contents}</codebase_file_contents>
        The current codebase file_tree: <file_tree>{file_tree}</file_tree>
        The user request: <request>{user_request}</request>
        Think deeply and step-by-step.
        """

    task_list_json = generate_task_list(skip_plan=skip_plan, user_request=user_request, system_message=backend_changes_system_message, user_message=user_message, task_list_file_types=backend_types)
    process_task(task_list_json)
    
    # validate_all_files_in_directory(BACKEND_PROJECT_DIR)
    total_tokens_used['total_cost'] = calculate_cost(total_tokens_used)
    logger.info(f"Total tokens used so far: {total_tokens_used}")

def generate_and_execute_cURL_requests(index0=False):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    # switch_git_branch(branch_name=f"task-{TASK_ID}")

    # git_status_result = subprocess.run(["git", "status"], capture_output=True, text=True)
    # logger.info(f"Git status result: {git_status_result.stdout}")

    file_tree, file_contents = read_directory(BACKEND_PROJECT_DIR)

    # Rebuild container and reset prisma
    time_of_last_deploy = datetime.now(timezone.utc).isoformat()
    # deploy_to_dokku(project_id=PROJECT_ID, type="backend", branch=f"task-{TASK_ID}") ## REMOVAL
    dokku_run_command(app_name=f"{PROJECT_ID}", command="cd /usr/src/app/backend && npx prisma db push --force-reset")

    latest_curl_request_list_contents = read_file_content(os.path.join(ROOT_PROJECT_DIR, "cURL_requests.json"))

    system_message_cURL_request_gen = "You are a brilliant and meticulous engineer. Generate cURL requests for the given codebase. The purpose is API Testing in which the functionality of the backend codebase will be tested. Ensure that the cURL requests work procedurally and if there are is any Authorization needed (in form of Tokens for example) in a request, they are obtained with earlier cURL requests. It should be one complete test flow of the complete API BackendRoutes – one request building up on another. Think step-by-step of all the cURL requests. Rules: \n- The Base URL for the cURL tests is not localhost:8080 instead its STRICTLY 'https://" + str(PROJECT_ID) + "-api.app.softgen.ai/' \n-Ensure that the cURL requests work procedurally and if there are is any Authorization needed (in form of Tokens for example) in a request, they are obtained with earlier cURL requests. It should be one complete test flow of the complete API BackendRoutes – one request is building up on another. \n- In that sense make absolutely sure to provide all the needed dynamic data for all the requests, all the relevant IDs, Tokens, Parameters, Variables, Headers, Credentials, Payloads, time-sensitive, etc... For example for a Chat room you need multiple users – in that case create more, # \n- If you create a curl request with the DELETE method, it can be confusing and may cause subsequent requests to fail. So make sure to place the DELETE request at the end of the list to avoid any subsequent requests that rely on it from failing... \n-Make sure to consider that if we need dynamic data, multiple entities to test a codebases full functionality and to run the cURL request to create multiple entities - apply that logic to suitable cases. \n Respond in this JSON Format: \n\n{\n  \"cURL List\": [\n    {\n      \"Index\": \"Here comes the Index of the cURL command in the sequence. Initial value = 0. Start with Index 0.\",\n      \"cURL_command\": \"The cURL command\",\n      \"received_Response\": \"The response received from executing the cURL command, empty by default\"\n    }\n  ]\n}" #Optionally if you want to test after the deletion, you can, create a new CREATE request after executing the DELETE request, as the previous data will be deleted.

    # Define the messages to be sent to the LLM API to generate cURL requests
    messages = [
        {
            "role": "system",
            "content": f"{system_message_cURL_request_gen}"
        },  # So consider edge cases, creating multiple entities
        {
            "role": "user",
            "content": f"<current_codebase_filetree>{str(file_tree)}</current_codebase_filetree> \n <current_codebase_contents>{str(file_contents)}</current_codebase_contents> " #\n Here the latest, potentially outdated cURL request list: <latest_curl_requests_list>{str(latest_curl_request_list_contents)}</latest_curl_requests_list>
        } 
    ]

    # Make an API call to generate the cURL requests and validate the JSON response
    response = make_llm_api_call(messages, json_output=True)
    cURL_requests_json = response.choices[0].message['content']
    cURL_requests = is_valid_json(cURL_requests_json)
    logger.info(f"cURL requests generated: {cURL_requests_json}")

    # Initialize the index for iterating through the cURL requests and a counter for each index
    index = 0
    index_counter = {}

    # Loop through the cURL requests and execute them
    while index < len(cURL_requests['cURL List']):
        # Check if the current index has been run 5 times
        # if index_counter.get(index, 0) >= 5:
        #     user_input = input("Index {} has been executed 5 times. Do you want to continue? (y/n): ".format(index))
        #     if user_input.lower() != 'y':
        #         break  # Exit the loop if user decides not to continue
        #     else:
        #         index_counter[index] = 0  # Reset the counter for this index if user wants to continue

        # Increment the counter for the current index
        index_counter[index] = index_counter.get(index, 0) + 1

        # Sort the cURL requests by their index to ensure they are executed in the correct order
        cURL_requests['cURL List'].sort(key=lambda x: x['Index'])
        request = cURL_requests['cURL List'][index]

        cURL_requests_json = response.choices[0].message['content']
        cURL_requests = is_valid_json(cURL_requests_json)
        logger.info(f"""Starting new iteration with updated cURL_requests list: {cURL_requests}""")

        # Add HTTP status code output to the cURL command
        cURL_command = request['cURL_command'] + ' -w "%{http_code}"'
        logger.info(f"""Executing cURL command number {index}: {cURL_command}""")
        # Execute the cURL command and capture the output
        cURL_process = subprocess.Popen(cURL_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)

        # Read the output from the cURL command and decode it
        cURL_output, cURL_error = cURL_process.communicate(timeout=30)
        decoded_cURL_output = cURL_output.decode('utf-8')
        # Extract the HTTP status code from the output
        http_status_code = decoded_cURL_output[-3:]

        # Combine the cURL output and error for error analysis
        combined_error_output = decoded_cURL_output + cURL_error.decode('utf-8')

        # Check for errors in the cURL output or HTTP status code
        is_error_present = 'Error' in combined_error_output or '<!DOCTYPE html>' in combined_error_output
        http_error = http_status_code.startswith('4') or http_status_code.startswith('5')
        if is_error_present or http_error:
            logger.error(f"cURL command failed with HTTP status code: {http_status_code}, error: {combined_error_output}")
        else:
            logger.info(f"cURL command succeeded with output: {decoded_cURL_output}")

        # Check the server logs for errors
        server_log = dokku_get_server_log(app_name=f"{PROJECT_ID}-backend", since=time_of_last_deploy)
        if server_log['has_errors']:
            server_log_error = server_log
            logger.info(f"Error detected: {server_log_error}")
        else:
            server_log_error = None


        # Process Errors in cURL requests
        if 'Error' in combined_error_output or '<!DOCTYPE html>' in combined_error_output or http_status_code.startswith('4') or http_status_code.startswith('5') or server_log_error:

            received_response_analysis_message = [
                {
                    "role": "system",
                    "content": "Analyze the received error message of the current cURL request and determine if the error is codebase related or if it's related to the cURL request itself (e.g., the request was incorrectly written, contains wrong information, incorrect IDs, JWT Tokens, invalid value). Or decide if the issue is with the codebase code and the code is not working as expected. If you are not sure what the root issue is default to request_related = false and handle it with codebase fixxing. Only if you are 100% certain and you see based on the given information that its an error with the request set it to TRUE. If the error is 'an integer, but a string was provided', 'Invalid value provided. Expected Int, provided String.' or similar, the majority of times its an codebase error -> put request_related False. Provide your answer as JSON output with 'request_related' set to TRUE OR False and, and if TRUE, populate 'Instructions' with the necessary changes or information to change the cURL requests List so that the Testing is successful. Use the following JSON structure for the output: { 'request_related': <TRUE_OR_FALSE'>, 'Instructions': <DETAILED_INSTRUCTIONS_TO_FIX_THE_CURL_REQUEST> }. Make sure to output a complete, valid JSON."
                }, #Ensure that the Task List actually works and propperly tests everything, that it doesnt forget to create something before it gets its, make sure its logical and consistence and include that in your instructions
                {
                    "role": "user",
                    "content": f"""
                    <request>{request}</request>
                    <error_message>{combined_error_output}</error_message>
                    <current_curl_requests_list>{json.dumps(cURL_requests)}</current_curl_requests_list>
                    """
                }
            ]
            response_analysis_response = make_llm_api_call(received_response_analysis_message, json_output=True)#useSmallerModel=True
            response_analysis_result = response_analysis_response.choices[0].message['content']
            response_analysis_json = json.loads(response_analysis_result)
            logger.info("Response analysis result" + response_analysis_result)

            if response_analysis_json.get('request_related', True):
                if 'invalid token' in combined_error_output.lower() or 'invalid id' in combined_error_output.lower() or 'id invalid' in combined_error_output.lower() or 'token invalid' in combined_error_output.lower():

                    messages = [
                        {
                            "role": "system",
                            "content": "You are the cURL requests List Authorization corrector. A cURL request failed with an error. The Authorization in the cURL request needs to be corrected. Your goal is to regenerate the cURL request list with the corrected Authorization so that the cURL request goes through. Ensure that the tokens used in the requests are correct by examining the 'received_Response' of /users or similar API requests where JWT tokens were received. Analyse the cURL request list and look at which user email has received which Token/ID and where that Token/ID was used again in subsequent requests, then analyse which Token/ID you need for the current cURL request list to fix the error. Rule: DO NOT ADD NEW CURL REQUESTS, KEEP THE REQUESTS IN THE SAME INDEX ORDER – you can ONLY change TOKEN / ID information. Analyse the used Token of the current request in utmost detail, comparing them character after character – to see if there are any differences - if there are correct the token of the current request with the right token from a previous request. 1.Start from the first character of Token 1 and compare it with the corresponding character in Token from the current failing request.\n 2.Move one character at a time along both tokens, comparing each pair of characters.\n 3.If a difference is found, note the position and the differing characters.\n 4.Continue the sequential comparison process to check for more differences further along in the tokens.\n 5.Keep track of all the differences found during the comparison, the exact characters that differ and their positions in the tokens.\n 6.Double-check the recorded differences to ensure accuracy and avoid any oversight or misidentification of differences. 7. Output the Final corrected Json cURL requests list."
                        },
                        {
                            "role": "user",
                            "content": f"<current_cURL_request>{request}</current_cURL_request> was executed with error <error>{combined_error_output}</error>. This is the complete cURL_requests_list <cURL_requests>{cURL_requests}</cURL_requests> Analyse if we are sending wrong Data in the current cURL request and need to change the current cURL request data, you see correct Authorization Tokens in the cURL request before the current one. Ensure that the tokens used in the requests are correct by examining the 'received_Response' of /users or similar API requests where JWT tokens were received.Fix the current cURL_request if possible. 1.Start from the first character of Token 1 and compare it with the corresponding character in Token from the current failing request.\n 2.Move one character at a time along both tokens, comparing each pair of characters.\n 3.If a difference is found, note the position and the differing characters.\n 4.Continue the sequential comparison process to check for more differences further along in the tokens.\n 5.Keep track of all the differences found during the comparison, the exact characters that differ and their positions in the tokens.\n 6.Double-check the recorded differences to ensure accuracy and avoid any oversight or misidentification of differences. 7. Output the Final corrected Json cURL requests list. Rule: DO NOT ADD NEW CURL REQUESTS, KEEP THE REQUESTS IN THE SAME INDEX ORDER – you can ONLY change Token values. Output in the same JSON Format and make sure to keep all the Data and dont change any information in a completed object (previous Index): \n\n{{\n  \"cURL List\": [\n    {{\n      \"Index\": \"Index of the cURL command in the sequence\",\n      \"cURL_command\": \"The cURL command\",\n      \"received_Response\": \"The response received from executing the cURL command\"\n    }}\n  ]\n}}"
                        }
                    ]

                    response = make_llm_api_call(messages, json_output=True)
                    cURL_requests_json = response.choices[0].message['content']
                    cURL_requests = is_valid_json(cURL_requests_json)
                    logger.info(f"cURL requests generated: {cURL_requests_json}")
                    index = index     

                else:
                    # Extract 'Instructions' from the response_analysis_json and ensure it's in JSON string format
                    instructions = response_analysis_json.get('Instructions')
                    if isinstance(instructions, dict):
                        instructions = json.dumps(instructions)
                    elif instructions is None:
                        instructions = "{}"

                    # logger.info(instructions)
                    
                    file_tree, file_contents = read_directory(BACKEND_PROJECT_DIR)

                    messages = [
                        {
                            "role": "system",
                            "content": f"{system_message_cURL_request_gen}."
                        },
                                {
                            "role": "user",
                            "content": f"We just received this error message <error>{combined_error_output}</error> for this cURL request: <request>{request}</request>. \n This is the current complete cURL_requests_list <cURL_requests_list>{json.dumps(cURL_requests)}</cURL_requests_list> \n\nUpdate it with these Instructions and try to fix the received error: <instructions>{instructions}.</instructions> <current_codebase_filetree>{str(file_tree)}</current_codebase_filetree> \n <current_codebase_contents>{str(file_contents)}</current_codebase_contents> \n"
                        },
                    ]

                    response = make_llm_api_call(messages, json_output=True)
                    cURL_requests_json = response.choices[0].message['content']
                    cURL_requests = is_valid_json(cURL_requests_json)
                    logger.info(f"cURL requests generated: {cURL_requests_json}")

                    index = 0
                    time_of_last_deploy = datetime.now(timezone.utc).isoformat()
                    dokku_run_command(app_name=f"{PROJECT_ID}", command="cd /usr/src/app/backend && npx prisma db push --force-reset")

                    time.sleep(4)


            else:
                # switch_git_branch(branch_name=f"task-{TASK_ID}")

                # git_status_result = subprocess.run(["git", "status"], capture_output=True, text=True)
                # logger.info(f"Git status result: {git_status_result.stdout}")
                
                # Handle errors related to the codebase
                logger.info(
                    "Handle errors related to the codebase. Generating an implementation plan to fix the error.")
                # Prepare the user request for generating an implementation plan to fix the error

                logger.info(f"cURL_command:{cURL_command}\n Combined_output_error{combined_error_output} Server_log_error:{server_log_error}\n ")
                user_request = f"I am currently running this list of cURL requests to test all the API BackendRoutes <cURL_requests>" + str(cURL_requests) + "</cURL_requests>. stuck at this cURL: <cURL_command>"+str(cURL_command)+"</cURL_command> - with cURL output: <cURL_command_output>"+str(decoded_cURL_output)+"</cURL_command_output> and received errors: "+str(combined_error_output)+" and server log error:"+str(server_log_error)+". FIX THE ERROR COMPLETELY. Think step by step." 

                system_message = f"You are skilled at troubleshooting and resolving issues in existing codebases. Your approach should include:\n\n- Analyzing the the cURL request list, the cURL request, the error(s) and the existing codebase. \n- Evaluating if the {backend_types} should be modified to resolve the error and creating instructions for modifications to these components to fix the error, with detailed explanations for each instruction. \n- Also be aware that is an incomplete codebase, that still being developed and it maybe that the issue is arising due to that. Inspect the cURL_requests List, if the cURL_request List is missing any functionality-crucial endpoints and logic to complete all of the cURL request and they have not been developed yet, create instructions to develop these new needed Endpoints or Logic-modifications in order to resolve the issue. \n- Identify and articulate the interdependencies between different system components, emphasizing how changes in one area might affect others. \n- Do not assign research or investigaton assignments, you have all the content. Rather provide concrete decisions on what to change and modify to fix the issue.\nYour goal is to deliver a comprehensive, actionable, technical sound plan to fix the issue. You can only modify files of the the type schema.prisma, BackendRoutes, BackendControllers, BackendServices, BackendMiddleware, BackendUtils, app.js and you have to complete the error resolution only within these file_types."

                user_message = "" + str(user_request) + "This is the <current_codebase_filetree>" + str(file_tree) + "</current_codebase_filetree> \n and these are the complete Codebase contents <current_codebase_contents>" + str(file_contents) + "</current_codebase_contents> \n- Do not assign research assignments, you have all the content. Rather provide concrete decisions on what to change and modify to fix the issue. Now fix the issue."

                # Execute Implementation Plan
                file_tree, file_contents = read_directory(BACKEND_PROJECT_DIR)
                task_list_json = generate_task_list(user_request, system_message, user_message, task_list_file_types=backend_types)
                process_task(task_list_json)
                # validate_all_files_in_directory(BACKEND_PROJECT_DIR)

                if index0:
                    # Generate new cURL requests – same as in the start of the function. To get up 2 date with the codebase.
                    messages = [
                        {
                            "role": "system",
                            "content": f"{system_message_cURL_request_gen}"
                        },

                        {
                            "role": "user",
                            "content": f"<current_codebase_filetree>{str(file_tree)}</current_codebase_filetree> \n <current_codebase_contents>{str(file_contents)}</current_codebase_contents> "
                        }  
                    ]

                    response = make_llm_api_call(messages, json_output=True)
                    cURL_requests_json = response.choices[0].message['content']
                    cURL_requests = is_valid_json(cURL_requests_json)
                    logger.info(f"cURL requests generated: {cURL_requests_json}")
                    logger.info(f"Attempted fix implemented. Now reattempting to run the cURL requests.")
                    index = 0
                    dokku_run_command(app_name=f"{PROJECT_ID}", command="cd /usr/src/app/backend && npx prisma db push --force-reset")

                else:
                    index = index

                time_of_last_deploy = datetime.now(timezone.utc).isoformat()

        # Update cURL requests List with successfully executed cURL request
        else:
            # logger.info(f"No errors present. Updated cURL request with received response: {request}")
            request['received_Response'] = cURL_output.decode('utf-8') if cURL_output else cURL_error.decode('utf-8')

            # Manually update the cURL_requests list with the received_Response for the current request
            for cURL_request in cURL_requests['cURL List']:
                if cURL_request['Index'] == request['Index']:
                    cURL_request['received_Response'] = request['received_Response']

            received_response_analysis_message = [
                {
                    "role": "system",
                    "content": "Analyze the received response of the current cURL request and determine if it contains new dynamic data that should be used to update subsequent cURL requests in the list. Dynamic Data is most often marked by xml tags similar to this <DYNAMIC_DATA_CONTENT_IS_LIKE_THIS>, if we have received information in the received_response update the subsequent requests with the new information. Make sure that all the Dynamic Data is 100% perfect. Make sure that the JWT matches those received from /users or similar API requests where JWT tokens were received in received_response. If a DELETE Method request is being analysed you should not change or delete previous curl requests. If applicable directly include multiple indexes to be updated. Provide a JSON output with 'update_needed' set to TRUE or FALSE and if true, populate 'Instructions' with the necessary changes to the cURL requests list. State the exact Index Number with the exact instructions on what to change. Think step by step. Use the following JSON structure for the output: { 'update_needed': <TRUE_OR_FALSE>, 'Instructions': { 'Index': [<INDEX_NUMBERS>], 'Changes': [<DETAILED_INSTRUCTIONS PER INDEX - STATE THE EXACT IDs, TOKENS, etc...] } }. Make sure to output a complete, valid JSON."
                },
                {
                    "role": "user",
                    "content": f"""
                    <request>{request}</request>
                    <received_response>{request['received_Response']}</received_response>
                    <current_curl_requests_list>{json.dumps(cURL_requests)}</current_curl_requests_list>
                    """
                }
            ]
            response_analysis_response = make_llm_api_call(received_response_analysis_message, json_output=True, max_tokens=500)#useSmallerModel=True
            response_analysis_result = response_analysis_response.choices[0].message['content']
            response_analysis_json = json.loads(response_analysis_result)
            logger.info("Response analysis result" + response_analysis_result)

            if response_analysis_json.get('update_needed', True):

                # Extract 'Instructions' from the response_analysis_json and ensure it's in JSON string format
                instructions = response_analysis_json.get('Instructions')
                if isinstance(instructions, dict):
                    instructions = json.dumps(instructions)
                elif instructions is None:
                    instructions = "{}"

                logger.info(instructions)

                messages = [
                    {
                        "role": "system",
                        "content": "You are a brilliant and meticulous engineer. Update the cURL requests JSON."
                    },
                    {
                        "role": "user",
                        "content": f"We just have completed this cURL request now: <request>{request}</request>. \n This is the current complete cURL_requests_list <cURL_requests_list>{json.dumps(cURL_requests)}</cURL_requests_list> \n\nUpdate the Dynamic Data with these Instructions: <instructions>{instructions}.</instructions> Dynamic Data is most often marked by xml tags similar to this <DYNAMIC_DATA_CONTENT_IS_LIKE_THIS>, update the subsequent requests with the new information. Make sure that all the Dynamic Data is 100% perfect. Make sure that the JWT matches those received from /users or similar API requests where JWT tokens were received in received_response. \n \n The objective here is to update the subsequent cURL requests in cURL requests List with the newely received_Response data from the current request. Common cases are Authentication tokens or session IDs (Store these to maintain an authenticated session with future cURL requests.) Resource identifiers (Store unique IDs to reference or retrieve corresponding resources in future cURL requests.), temporary state or context (Preserve pagination details, transaction IDs, or workflow progress indicators for subsequent cURL requests) or other data we are dependent on in the incoming cURL_requests. Edit future, subsequent requests in the cURL requests list to contain this data in the request. Also make sure the cURL_requests_list contains correct Authorization Tokens <cURL_requests>{json.dumps(cURL_requests)}</cURL_requests>. Ensure that the tokens used in the requests are correct by examining the 'received_Response' of /users or similar API requests where JWT tokens were received. Follow these steps to validate and implement the correct tokens: \n1. Start from the first character of the token in the previous successful authentication request.\n2. Compare it with the corresponding character in the token from the current failing request.\n3. Move one character at a time along both tokens, comparing each pair of characters.\n4. If a difference is found, note the position and the differing characters.\n5. Continue the sequential comparison process to check for more differences further along in the tokens.\n6. Keep track of all the differences found during the comparison, the exact characters that differ, and their positions in the tokens.\n7. Double-check the recorded differences to ensure accuracy and avoid any oversight or misidentification of differences.\n8. Replace the incorrect token with the correct one from the previous successful authentication response.\n9. Output the Final corrected Json cURL requests list. Rule: \n- DO NOT RANDOMLY CHANGE THE JWT TOKENS FROM COMPLETED REQUESTS. \n- KEEP THE COMPLETE TOKEN/ID/etc..., UNDER NO CIRCUMSTANCE SHORTEN IT. \n-DO NOT ADD NEW CURL REQUESTS, KEEP THE REQUESTS IN THE SAME INDEX ORDER. Output in the same JSON Format and make sure to keep all the Data and dont change any information in a completed object (previous Index): \n\n{{\n  \"cURL List\": [\n    {{\n      \"Index\": \"Index of the cURL command in the sequence\",\n      \"cURL_command\": \"The cURL command\",\n      \"received_Response\": \"The response received from executing the cURL command\"\n    }}\n  ]\n}}"
                    }
                ]

                response = make_llm_api_call(messages, json_output=True)
                cURL_requests_json = response.choices[0].message['content']
                cURL_requests = is_valid_json(cURL_requests_json)
                logger.info(f"cURL requests generated: {cURL_requests_json}")
                index += 1

            else:
                index += 1


        if index == len(cURL_requests['cURL List']):
            # Remove the current cURL requests JSON file if it exists
            cURL_requests_file_path = os.path.join(ROOT_PROJECT_DIR, 'cURL_requests.json')
            if os.path.exists(cURL_requests_file_path):
                os.remove(cURL_requests_file_path)

            # Create and save the last cURL requests JSON
            with open(cURL_requests_file_path, 'w') as file:
                json.dump(cURL_requests, file, indent=4)
                logger.info("Saved cURL requests to cURL_requests.json in ROOT_PROJECT_DIR")
            break

        total_tokens_used['total_cost'] = calculate_cost(total_tokens_used)
        logger.info(f"Total tokens used so far: {total_tokens_used}")

def generate_mock_data(gen=True):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    dokku_run_command(app_name=f"{PROJECT_ID}", command="cd /usr/src/app/backend && npx prisma db push --force-reset")
    time_of_last_deploy = datetime.now(timezone.utc).isoformat()    

    if gen:
        logger.info(
            "Creating Database Seed file for mock data.")
        # current_api_doc_content = read_file_content("API_doc.md")
        seed_js_gen_task = {
            "Request": "Create a comprehensive seed.js file.",
            "Task List": [
                {
                    
                    "Task": {
                        "TaskID": "1",
                        "File": {
                            "FileName": "seed.js",
                            "Type": "seed.js",
                            "Operation": "MODIFY",
                            "Instructions": "Create a comprehensive seed.js file for the Prisma DB Schema that encapsulates the entire application logic. The seed file should include extensive mock data for all models defined in the schema.prisma file, ensuring that the data is representative of real-world scenarios for testing and demonstration purposes. Follow these steps:\n\n1. Review the schema.prisma file to understand the data models and relationships.\n2. For each model, generate mock data that adheres to the constraints and validations defined in the schema.\n3. Ensure that relationships between models are respected in the mock data, including one-to-one, one-to-many, and many-to-many relations.\n4. Use the Prisma Client API to insert the mock data into the database, handling any potential errors or exceptions. \n\nRemember to:\n- Include a diverse range of data that covers different use cases and edge cases.\n- Keep the seed.js file organized. If there are Image uploads strictly use https://placehold.co/(needed-resolution-width)x(needed-resolution-height)?text=Any+Text+here as example URLS. ",
                            "FileDependencies": [],
                        }
                    }
                }
            ]
        }
        process_task(json.dumps(seed_js_gen_task))
        

   # DB Seeding
    logger.info(
        "Starting DB seeding process.")
    while True:
            try:
                seed_result = dokku_run_command(app_name=f"{PROJECT_ID}", command="cd /usr/src/app/backend && npm run seed")

                if "error" in seed_result.keys():
                    logger.error(f"DB seed encountered an error: {seed_result['error']}")
                    # Send to generate_task_list to be fixed
                    user_request = f"I just executed npm run seed & received an error {seed_result['error']}"
                    file_contents_seed = read_file_content(os.path.join(BACKEND_PROJECT_DIR, "prisma", "seed.js"))
                    file_contents_schema = read_file_content(os.path.join(BACKEND_PROJECT_DIR, "prisma", "schema.prisma"))

                    task_list_json = generate_task_list(
                        user_request=user_request, 
                        system_message="You are a skilled software developer competent at troubleshooting and resolving issues for prisma/seed.js files.", 
                        user_message="<error>" + str(user_request) + "</error>\n <current-seed.js-file>" + str(file_contents_seed) + "</current-seed.js-file> \n <current-schema.prisma-file>" + str(file_contents_schema) + "</current-schema.prisma-file> \n. Approach this methodically, focusing on detailed troubleshooting and resolution. ", task_list_file_types="seed.js")

                    process_task(task_list_json)
                    continue
                else:
                    logger.info("DB seed executed successfully.")
                    break
            except Exception as e:
                logger.error(f"Failed to start DB seed: {str(e)}")
                continue

    def gen_seeded_users_json():
        global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
        
        file_contents_seed = read_file_content(os.path.join(BACKEND_PROJECT_DIR, "prisma", "seed.js"))
        messages = [
            {
                "role": "system",
                "content": "You will receive the seed.js file contents, you have to extract all of the users, their login data, with their respective roles in order to create a users.json Testing Fixture. Make sure to include one user per each user role – so that all role authentication are covered. If only one user role exists, then stick to one login, if multiple roles exist make sure to include authentications for all roles. Extract that information from the seed.js and return it. Output as JSON. Only output the Users with their Login Data and respective role (if the role exists). DO NOT output any extra comments. DO NOT output any role if there is no role mentioned – DO NOT INCLUDE IT!! Keep the JSON Structure as simple as possible  "
            },  
            {
                "role": "user",
                "content": f"The seed.js file contents: {file_contents_seed}"
            } 
        ]
        response = make_llm_api_call(messages, json_output=True)
        seeded_users_with_roles = response.choices[0].message['content']
        with open(os.path.join(ROOT_PROJECT_DIR, "users.json"), 'w') as file:
            file.write(seeded_users_with_roles)
        # with open(os.path.join(FRONTEND_PROJECT_DIR, "cypress", "e2e", "users.json"), 'w') as file:
        #     file.write(seeded_users_with_roles)
        logger.info("Seeded users with roles have been saved to file.")

def generate_API_doc():
    logger.info(
        "Creating API Documentation file with the latest codebase contents.")
    # current_api_doc_content = read_file_content("API_doc.md")
    api_doc_gen_task = {
        "Request": "Create a comprehensive API_doc.md.",
        "Task List": [
            {
                "Task": {
                    "TaskID": "1",
                    "File": {
                        "FileName": "API_doc.md",
                        "Type": "API_doc.md",
                        "Operation": "MODIFY",                        
                        "Instructions": " Instructions are in @generate_code_newFileContents – this is just a placeholder.",
                        "FileDependencies": []
                    }
                }
            }
        ]
    }
    process_task(json.dumps(api_doc_gen_task))


# =========================
# Frontend systems
# =========================

frontend_file_type_prompt = """    
Frontend file_type definitions and planning rules:    
    
    **FrontendApi**
    - *DESCRIPTION*: Manages all interactions with the backend API, including HTTP requests and responses.
    - *INTERACTION FLOW / DEPENDENCIES*: Serves as a service layer without direct interaction with other frontend types. 
        - Provides data to FrontendPages and FrontendHooks through API calls.
        - These are THE ONLY FILES in which API calls are allowed to be made.
    - *NAMING CONVENTION*: API functions should be named according to the topic of data they handle: 'projectsApi', 'tasksApi', etc... Use camelCase. 
    - *HOW TO IMPORT*: Import FrontendAPI from @api/(frontendApiFileName.jsx)
    - *IMPLEMENTATION-PLAN WRITING APPROACH*:
        - Review the backend API documentation to understand the available endpoints and their requirements.
        - Define API functions that correspond to the backend endpoints, ensuring they are modular and reusable.
        - Implement error handling within API functions to manage potential request failures gracefully.
        - Document the API functions, specifying the expected parameters, return types, and any errors they may throw.

    **FrontendUtils**
    - *DESCRIPTION*: A collection of general utility functions that assist in various tasks across the frontend.
    - *INTERACTION FLOW / DEPENDENCIES*: Used as needed by other frontend types such as FrontendComponents, and FrontendHooks.
        - Provides commonly needed functionality like data formatting, validation, or local storage management.
    - *NAMING CONVENTION*: Utility functions should have clear and descriptive names that indicate their purpose: `formatDate`, `validateEmail`, `storeInLocalStorage`. Use camelCase
    - *HOW TO IMPORT*: Import Utils from @utilities/(utilsFileName.jsx)
    - *IMPLEMENTATION-PLAN WRITING APPROACH*:
        - Identify common tasks and operations that can be abstracted into utility functions to avoid code duplication.
        - Create utility functions with clear inputs and outputs, ensuring they are pure and side-effect free whenever possible.
        - Document each utility function, describing its purpose, parameters, and return values.

    **FrontendHooks**
    - *DESCRIPTION*: Custom hooks for encapsulating logic and state management in a reusable manner.
    - *INTERACTION FLOW / DEPENDENCIES*: Interacts With: FrontendApi, FrontendUtils, FrontendContexts
        - Hooks interact with FrontendApi for data fetching or sending requests.
        - Hooks use FrontendUtils for general-purpose functions like formatting data or managing local storage.
        - Hooks can consume FrontendContexts to access shared state or functions.
    - *NAMING CONVENTION*: Hooks should be named starting with 'use' followed by a description of their purpose: `useFeatureName`. Examples: `useAuth`, `useUserProfile`, `useForm`. Use camelCase.
    - *HOW TO IMPORT*: Import Hooks from @hooks/(hookFileName.jsx)
    - *IMPLEMENTATION-PLAN WRITING APPROACH*:
        - Analyze the component or feature that requires state management or logic encapsulation to determine the hook's responsibilities.
        - Plan the hook's structure, including its return values and any parameters it accepts.
        - Consider the hook's interaction with FrontendApi for data fetching and ensure efficient and error-handled communication with the backend.
        - Utilize FrontendUtils within the hook for any necessary utility functions, ensuring a clean separation of concerns.
        - Document the hook's functionality, parameters, and return values, providing clear usage examples.

    **FrontendContexts**
    - *DESCRIPTION*: Contexts provide a way to pass data through the component tree without having to pass props down manually at every level.
    - *INTERACTION FLOW / DEPENDENCIES*: Interacts With: FrontendComponents, FrontendHooks
        - Contexts are consumed by components or hooks that require access to shared data or state.
    - *NAMING CONVENTION*: Contexts should be named with a 'Context' suffix to indicate that they provide a React context: `AuthenticationContext`, `UserProfileContext`, `ThemeContext`. Use PascalCase.
    - *HOW TO IMPORT*: Import Contexts from @contexts/(ContextFileName.jsx)
    - *IMPLEMENTATION-PLAN WRITING APPROACH*:
        - Determine the scope of the data that needs to be accessible throughout the component tree.
        - Create a context using React's createContext API and define a provider component that encapsulates children components.
        - Ensure that the context provider is placed high enough in the component tree so that all components that need the data can access it.
        - Implement custom hooks, if necessary, to provide a clean and easy-to-use interface for context consumers.
        - Document the context, explaining what data is available, how to consume it, and any best practices for usage.

    **FrontendComponents**
    - *DESCRIPTION*: Specific UI components.
    - *INTERACTION FLOW / DEPENDENCIES*: Interacts With: FrontendUtils, FrontendHooks, FrontendContexts
        - Components use FrontendUtils for utility functions.
        - Components use FrontendHooks for logic and state handling.
        - Components can consume FrontendContexts to access shared state or functions.
    - *NAMING CONVENTION*: Components should have descriptive and unique names to avoid conflicts, as all components reside in a shared "@components" folder. Use PascalCase. The naming convention should clearly articulate the component's purpose or the context in which it is used:
        - For a sub-component that is used within a specific page, prefix the component name with the page name followed by the component's function: `PageNameSubComponent`. Examples: `HomePageHeader`, `AboutPageSidebar`, `ContactPageForm`.
        - For utility components or those shared across multiple pages, use a name that reflects their general utility: `UtilityName`. Examples: `NavigationBar`, `Footer`, `Modal`.
        - For components that are specific to a particular feature within the application, combine the feature name with the word 'Component': `FeatureNameComponent`. Examples: `UserProfileCard`, `ProductListView`, `OrderHistoryTable`.
    - *HOW TO IMPORT*: Import Components from @components/(ComponentFileName.jsx)
    - *IMPLEMENTATION-PLAN WRITING APPROACH*:
        - Start by analyzing the user's request, which may be incomplete or vague. Use your expertise to infer the underlying needs and define a clear, actionable objective for the component.
        - Envision the user experience and interface design, considering the flow and how users will interact with the component.
        - Content planning:
            1. Determine the type of content the component will display and how it should be structured.
            2. Plan for dynamic content and data binding, considering how the content will be updated or changed.
        - Layout and design planning:
            1. Sketch the main sections of the component and their interrelations, considering the visual hierarchy.
            2. Select a layout strategy (grid or flexbox) to organize the sections cohesively.
            3. Establish rules for spacing, alignment, and positioning of elements within the component.
            4. Ensure the design is scalable and responsive, adapting gracefully to various screen sizes.
            5. Integrate interactive elements thoughtfully, planning for user actions and feedback.
        - Select appropriate Chakra UI components to assemble the desired complex component, leveraging the library's composability.
        - Define the component's architecture:
            1. Specify the props the component will receive and the state it will manage.
            2. Plan for any context or data hooks the component might need to interact with.
        - Create a new file in the "@components" directory with a name that adheres to the established naming conventions.
        - Develop the component with Chakra UI, focusing on responsiveness and accessibility, and using Chakra's styling props for visual fine-tuning.
        - Document the component thoroughly, detailing its purpose, usage, and accepted props, supplemented by example implementations.
        - Strategize the integration of the new component within existing pages, ensuring compatibility with existing hooks and utilities and planning for any necessary refactoring.

    **FrontendPages**
    - *DESCRIPTION*: Serve as the main containers for each view or route in the application. They assemble everything to create the complete page.
    - *INTERACTION FLOW / DEPENDENCIES*: Interacts With: FrontendComponents, FrontendHooks, FrontendApi    
        - Pages use FrontendComponents for specific features within that page.
        - Pages utilize FrontendHooks for logic and state management.
        - Pages interact with FrontendApi to fetch or send data to the backend.
    - *NAMING CONVENTION*: Page file names should match the URL path they represent, following Next.js 13 filesystem-based routing. For example, a page accessible at '/about' should be named 'about.jsx', and a nested route like '/projects/[id]' should be represented by a file structure of 'projects/[id].jsx'. The 'index.jsx' file within a directory serves as the default page for the route corresponding to that directory. 
    - *EXAMPLE FILE NAMES*:
        - 'index.jsx' for the homepage
        - 'about.jsx' for the About page
        - 'contact.jsx' for the Contact page
        - 'faq.jsx' for the FAQ page
        - 'projects/index.jsx' for the main Projects page
        - 'projects/[id].jsx' for individual project details pages
        - 'blog/index.jsx' for the main Blog page
        - 'blog/posts/[year]/[month]/[slug].jsx' for individual blog post pages
        - 'blog/categories/index.jsx' for the Blog categories overview page
        - 'blog/categories/[category].jsx' for individual Blog category pages
        - 'login.jsx' for the Login page
        - 'signup.jsx' for the Signup page
        - 'user/profile/index.jsx' for the User Profile overview page
        - 'user/profile/edit.jsx' for the User Profile edit page
        - 'user/settings.jsx' for the User Settings page
    - *HOW TO IMPORT*: Do NOT import Pages. Never.
    - *IMPLEMENTATION-PLAN WRITING APPROACH*:
        - Begin by understanding the user's request and the purpose of the page within the application's flow. Use this insight to plan the layout and structure of the page.
        - Determine which FrontendComponents are needed to build the page's features and how they will work together to create a cohesive experience.
        - Plan the import statements for the required FrontendComponents, ensuring they are organized and maintainable.
        - Consider the interaction between FrontendHooks and FrontendComponents to manage state and logic within the page.
        - Ensure that the page's interaction with FrontendApi is clear and efficient for data fetching or sending.
        - Document the page's functionality and the interaction of its components, providing clear instructions for future developers.

    GENERAL RULES:
    - State the EXACT IMPORTS in the tasks
    - Be VERY specific in the tasks
    - Consider the interdependencies and think holistically.
    - Use static data only unless you have dynamic data served via an API from the API Documentation provided by the user. If the provided documentation is empty or does not contain the needed backend data, you HAVE TO use Static data. DO NOT CREATE ANY API FILES IF THERE IS NOT THE ASSOCIATED API DOCUMENTATION PROVIDED BY THE USER!
    - STRICTLY use named exports for each component or function. For example, define your component or function as export function MyComponent() { /*...*/ }. This allows for explicit imports and better tree-shaking during the build process.
    - DO NOT EDIT _app.jsx! UNDER NO CIRCUMSTANCE.

    - If a FileName is in a nested folder, then YOU MUST INCLUDE THE RELATIVE PATH FROM THE FILE_TYPE ROOT, for example, folder1/fileName.js or folder1/folder2/fileName.jsx. THIS IS VERY IMPORTANT!
    - Example for Next.js page routing: 
        - For a page in the root FrontendPages directory: index.jsx
        - For a nested page: projects/index.jsx or projects/overview.jsx
        - For a dynamic route: projects/[id].jsx
        - For a deeply nested page within subdirectories: user/profile/settings.jsx
        - For a dynamic route with multiple parameters: blog/posts/[year]/[month]/[slug].jsx
        - For a page with a catch-all route: blog/[...slug].jsx
        - For an optional catch-all route: blog/[[...slug]].jsx

    - Follow the outlined naming conventions and always articulate exactly what the file exactly does. 

"""

    # **FrontendStaticData**
    # - In cases where dynamic data is not received from the user (via API endpoints provided in documentation), then the necessary API endpoints do not exist and you have to create static data files `staticDataTopicFileName.js` containing static mock data to use.
    # - Its very important to create static data if needed with the FrontendStaticData file_type 
    # - Naming convention: if this static data needs to be coming via Backend API in the future append the file_name with "_be" -> "TopicFileName_be.js"  so the backend is notified that they need to create an API for this data. Use CamelCase.
    # - Import static data using the path `@staticData/(TopicFileName.js)`


def get_dynamic_route(page_route): #WIP TODO
    #make_llm_api_call
    # Messages 
    # System: You receives a page_route and the current database dump – then you have to create the accurate page_route with the correct id from the dynamic data so the page can be visited and tested.
    # User:     
    pass

frontend_changes_system_message=f"""
    As a distinguished Frontend Web Engineer now assuming the role of Product Owner, your mission is to orchestrate the integration of user requests into the existing Next.js 13 framework. Your objective is to craft a detailed and executable Task List that will guide the incorporation of user requests. Leveraging your expertise, you will distill user requests into a series of precise tasks, detailing modifications to Pages, Components, Global State management with Zustand, data fetching via SWR, and form management using React Hook Form. 
    Your plan will also encompass the utilization of Chakra UI for interface styling, Axios for performing HTTP requests, and the Next.js 13 Page Router for seamless navigation throughout the application.

    {frontend_file_type_prompt}

    # Step 1: Analyze the user request to determine the necessary components, pages, and state management features.
    - Identify the user's goals and the required functionality for each page and component.
    - Determine the data and state that needs to be managed globally and locally.

    # Step 2: Design the page structure and layout with Chakra UI components, considering Next.js features.
    - Layout Type: Choose between Grid and Flexbox based on content organization requirements, leveraging Next.js's built-in CSS support.
    - Sections: Construct structured areas like Header, Main Content, Sidebar, and Footer using Chakra UI's Box, Center, and Stack components.
    - Visual Hierarchy: Apply Chakra UI's design tokens for size, color, and spacing to create a clear visual flow.

    # Step 3: Organize the content using Chakra UI components, ensuring accessibility and responsiveness.
    - Content Types: Use Text, Heading for textual content, Image for visuals, and FormControl with form elements for user inputs.
    - Content Placement: Arrange content responsively using Grid or Flexbox, with consideration for different screen sizes.

    # Step 4: Plan interactive elements and behaviors using Chakra UI components, custom hooks, and Next.js Page Router.
    - Interactive Elements: Specify elements like Buttons and Links for user actions, utilizing the Link component from Next.js for client-side routing.
    - Expected Behaviors: Implement Modal for dialogues, and manage state transitions with appropriate hooks or state management libraries.

    # Step 5: Establish global state management, integrating with Next.js features.
    - State Management: Design the global state architecture, creating hooks for shared state access across components.

    # Step 6: Develop data fetching strategies with Axios and React Query for API interactions.
    - Data Fetching: Employ Axios for making HTTP requests and React Query for data fetching, caching, and revalidation.

    # Step 7: Generate a comprehensive Task List with detailed implementation plans for each task.
    - Task Breakdown: Formulate tasks for creating or modifying Pages, Components, and setting up data fetching strategies with Axios and React Query. Incorporate custom hooks for complex state logic or side effects.
    - Ensure all tasks consider the integration of Next.js, Chakra UI, Axios, and React Query for a cohesive development approach.

    - Available Chakra Components for usage: Aspect Ratio, Box, Center, Container, Flex, Grid, SimpleGrid, Stack, Wrap, Button, Checkbox, Editable, Form Control, Icon Button, Input, Number Input, Pin Input, Radio, Range Slider, Select, Slider, Switch, Textarea, Badge, Card, Code, Divider, Kbd, List, Stat, Table, Tag, Alert, Circular Progress, Progress, Skeleton, Spinner, Toast, Text, Heading, Highlight, Alert Dialog, Drawer, Menu, Modal, Popover, Tooltip, Accordion, Tabs, Visually Hidden, Breadcrumb, Link, LinkOverlay, SkipNav, Stepper, Avatar, Icon, Image, Close Button, Portal, Show / Hide, Transitions
    - Available Chakra Hooks for usage: useBoolean, useBreakpointValue, useClipboard, useConst, useControllable, useDimensions, useDisclosure, useMediaQuery, useMergeRefs, useOutsideClick, usePrefersReducedMotion, useTheme, useToken, Component, useCheckbox, useCheckboxGroup, useRadio, useRadioGroup, useSlider, useRangeSlider

    """

def frontend_changes(user_request, skip_plan=True):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    logger.info("Starting frontend dev.")
    file_tree, file_contents = read_directory(FRONTEND_PROJECT_DIR)
    current_api_doc_content = read_file_content(os.path.join(ROOT_PROJECT_DIR, "API_doc.md"))

    user_message=f"""
    The current codebase file contents: <codebase_file_contents>{file_contents}</codebase_file_contents>
    The current codebase file_tree: <file_tree>{file_tree}</file_tree>
    The current API Documentation: <api_doc>{current_api_doc_content}</api_doc>
    The request: <request>{user_request}</request>


    Think deeply and step-by-step. 
    Output a complete Implementation Plan. 
    Keep the implementation plan as simple as possible. 
    Make sure to structure the tasks correctly so that and givem then Index based on their file dependencies. It is crucial to ensure that if a task has a file dependency with a file being modified in another task, it is ranked correctly. This ensures that the changes to the dependent file are completed before working on the task that has the dependency. 

    """

    task_list_json = generate_task_list(user_request=user_request, user_message=user_message, system_message=frontend_changes_system_message, task_list_file_types=frontend_types, skip_plan=skip_plan)
    process_task(task_list_json)
    # install_fe_missing_packages()
    total_tokens_used['total_cost'] = calculate_cost(total_tokens_used)
    logger.info(f"Total tokens used so far: {total_tokens_used}")

def run_client_server_and_check_errors(user_request):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    # Scan the FRONTEND_PROJECT_DIR/src/pages/ directory for page files, including subdirectories
    page_routes = []

    for root, dirs, files in os.walk(os.path.join(FRONTEND_PROJECT_DIR, "src", "pages")):
        for f in files:
            if not f.startswith("_") and f.endswith((".js", ".jsx")):
                route = os.path.splitext(f)[0]  # Remove file extension
                if route == "index":  # Handle index files
                    if root == os.path.join(FRONTEND_PROJECT_DIR, "src", "pages"):
                        route = "/"  # Root index file should correspond to the base route '/'
                    else:
                        # Subdirectory index file should correspond to the subdirectory route
                        subdirectory = os.path.relpath(root, os.path.join(FRONTEND_PROJECT_DIR, "src", "pages"))
                        route = subdirectory.replace(os.path.sep, "/")
                else:
                    # Non-index files should include their subdirectory as a prefix
                    subdirectory = os.path.relpath(root, os.path.join(FRONTEND_PROJECT_DIR, "src", "pages"))
                    route = os.path.join(subdirectory, route).replace(os.path.sep, "/")
                # Handle dynamic routes by replacing [id], [name], [anyContent] with the number 1 --> # TODO implement sophisticated system that does more than replace with number 1
                route = re.sub(r'\[\w+\]', '1', route)
                page_routes.append(route)
    logger.info(f"Found the following page routes: {page_routes}")
    
    for page_route in page_routes:
        try:
            # Initialize error list
            errors = ['initial']
            # Loop until there are no more errors
            while errors:

                # Visit the page and check for console errors
                logger.info(f"Visiting and checking page {page_route}")
                errors = visit_and_check_page_errors(page_route)
                if errors:
                    logger.info(f"Errors found in the console for page {page_route}: {errors}")
                    # Attempt to fix the errors found in the console
                    fix_fe_errors_from_console(user_request, errors, page_route)
                else:
                    logger.info(f"No errors found in the console for page {page_route}")
                    break  # Exit loop if no errors
        except Exception as e:
            logger.error(f"Failed to run frontend server or check errors for route {page_route}: {str(e)}")

def visit_and_check_page_errors(page_route):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    # Visit the page using a headless browser, collect console logs, and read page content
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('window-size=1200x600')
    options.add_argument('--no-sandbox')  # Add no-sandbox argument to options
    options.add_argument('--disable-dev-shm-usage')  # Overcome limited resource problems
    options.add_argument('--disable-gpu')  # Disable GPU hardware acceleration
    options.add_argument('--remote-debugging-port=9222')  # Specify remote debugging port
    options.add_experimental_option('excludeSwitches', ['enable-logging'])  # Disable logging
    driver = webdriver.Chrome(options=options)
    driver.get(f"https://{PROJECT_ID}.app.softgen.ai/{page_route}")
    time.sleep(6)
    logs = driver.get_log('browser')

    # errors = [{'level': log['level'], 'message': log['message'], 'source': log['source']} for log in logs if log['level'] == 'SEVERE']
    errors = [log for log in logs if log['level'] == 'SEVERE']
    # Attempt to read the page content and include it in the errors if there are any visible error messages
    try:
        page_content = driver.find_element(By.TAG_NAME, 'body').text
        if "Error" in page_content:
            errors.append({'level': 'SEVERE', 'message': page_content, 'source': 'page-content'})
    except Exception as e:
        errors.append({'level': 'SEVERE', 'message': f"Failed to read page content: {str(e)}", 'source': 'page-content'})

    driver.quit()
    return errors

def fix_fe_errors_from_console(overarching_user_request, errors, page_route):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    logger.info(
        "Handle errors related to the codebase. Generating an implementation plan to fix the error.")
    # Prepare the user request for generating an implementation plan to fix the error
    # page_route_file_contents = construct_file_imports_contents_context(os.path.join(FRONTEND_PROJECT_DIR, "src", "pages", f"{page_route}.jsx"))
    file_tree, file_contents = read_directory(FRONTEND_PROJECT_DIR)
    current_api_doc_content = read_file_content(os.path.join(ROOT_PROJECT_DIR, "API_doc.md"))


    system_message=f"""You are an Expert Frontend Web Engineer, tasked with creating a detailed technical implementation plan to address and resolve errors that have occurred in the frontend codebase. Your goal is to deliver a comprehensive, actionable, and technically sound implementation plan that:
    - Analyzes the errors reported by the system, considering the existing codebase structure, to determine which {frontend_types} need to be created or modified to fix the errors.
    - Provides specific modifications or additions to these file_types to seamlessly resolve the errors, with clear explanations for each recommendation.
    - Identifies and articulates the interdependencies between different system components, highlighting how changes in one area might affect others, especially in the context of error handling and propagation.
    - Ensures that the error resolution is confined to the specified frontend file_types: {frontend_types}. All imports should be limited to these file types to maintain consistency and modularity.
    - Makes definitive statements about the steps required to integrate the error fixes, avoiding any ambiguity as the expert in frontend error resolution.
    - You DO NOT have to investigate the server error (status 500) – this is an general error thats associated with the other errors reported, as soon as the other errors are resolved this one will be to.
    - Recommends the use of well-known third-party packages for handling complex error cases, providing detailed instructions for their integration into the existing codebase.

    {frontend_file_type_prompt}

    It is autumn and you have to work extra hard to finish this up. You will get a big bonus payout when you finish it so you are extra motivated to work hard and smart on this. Think step by step and deeply to solve problems and come up with clever, pragmatic, and efficient solutions.
    """

    user_request = f"We encountered errors on the route {page_route}.jsx: <errors_list>For the page {page_route}.jsx has encountered the issue:" + str(errors) + f"</errors_list>. The errors have to be fixxed perfectly, solve the issues and fix the errors. The issues occur on '{page_route}.jsx' Page. Your focus is on fixxing the error only, the request information is only given as context for deeper understanding of the issue. Your main goal is to solve the issue."  #Durring the development of this request: <request>{overarching_user_request}</request> 

    user_message = f"""
        The current codebase file contents: <codebase_file_contents>{file_contents}</codebase_file_contents>
        The current codebase file_tree: <file_tree>{file_tree}</file_tree>
        The current API Documentation: <api_doc>{current_api_doc_content}</api_doc>
        The user request: <request>{user_request}</request>
        Think deeply and step-by-step.
        Output a complete Implementation Plan. 
        Keep the implementation plan as simple as possible. 
        Make sure to structure the tasks correctly so that and givem then Index based on their file dependencies. It is crucial to ensure that if a task has a file dependency with a file being modified in another task, it is ranked correctly. This ensures that the changes to the dependent file are completed before working on the task that has the dependency. 

    """ #    - The Backend API base url is always https://{PROJECT_ID}-api.app.softgen.ai, however you can only implement API Routes that are existing in the API Documentation for that route. if an API Documentation does not exist in the API Documentation, you cannot implement it. 



    # user_message = "<request>" + str(user_request) + " This is the API_documentation if there are any api_related issues <api_doc>" + str(current_api_doc_content) + " </api_doc>  <request> And these are the File Contents related to the page and the error. </current_file_contents>" + str(page_route_file_contents) + "</current_file_contents> and this is the File_tree of the Codebase, neeeded for you to fix potential import errors. </current_file_tree>" + str(file_tree) + "</current_file_tree> \n- Do not assign research assignments, you have all the file contents. Rather provide concrete decisions on what to change and modify to fix the issue. Now fix the issue."
    # user_message = "<request>" + str(user_request) + " This is the API_documentation if there are any api_related issues <api_doc>" + str(current_api_doc_content) + " </api_doc>  <request> And these are the complete Codebase contents of the frontend <current_frontend_codebase_contents>" + str(file_contents) + "</current_frontend_codebase_contents>. \n- Do not assign research or investigation assignments, you have all the file contents. Rather provide concrete decisions on what to change and modify to fix the issue. Now fix the issue."

    # Execute Implementation Plan
    task_list_json = generate_task_list(user_request, system_message, user_message, task_list_file_types=frontend_types, skip_plan=True)
    process_task(task_list_json)
    # install_fe_missing_packages()




# =========================
# End to End systems
# =========================

def execute_single_file_edit(user_request, file_name, file_type, file_operation): 

    # Todo Add custom Instructions generation for better results
    single_task = {
        "Request": f"{user_request}.",
        "Task List": [
            {
                
                "Task": {
                    "TaskID": "1",
                    "File": {
                        "FileName": f"{file_name}",
                        "Type": f"{file_type}",
                        "Operation": f"{file_operation}",                        
                        "Instructions": f"{user_request}",
                        "FileDependencies": []
                    }
                }
            }
        ]
    }
    process_task(json.dumps(single_task))


def gen_selected_file_changes(user_request, file_name, file_type, skip_plan):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    file_path = get_file_path_by_type(file_name, file_type)
    file_contents = read_file_content(file_path)
    imports_file_contents = extract_imports_from_file_contents(file_contents, depth=0, max_depth=1)

    if file_type.startswith("Frontend"):
        system_message = frontend_changes_system_message
        file_tree, fe_complete_contents = read_directory(FRONTEND_PROJECT_DIR)
        file_types=frontend_types
    elif file_type.startswith("Backend") or file_type in ["app.js", "schema.prisma"]:
        system_message = backend_changes_system_message
        file_tree, be_complete_contents = read_directory(BACKEND_PROJECT_DIR)
        file_types=backend_types

    user_message=f"""
        The {file_name} file contents: <{file_name}_file_contents>{file_contents}</{file_name}_file_contents>
        The imports file contents: <imports_file_contents>{imports_file_contents}</imports_file_contents>
        The current codebase file tree: <file_tree>{file_tree}</file_tree>
        The user request: <request>{user_request}</request>
        Think deeply and step-by-step.
        """

    task_list_json = generate_task_list(skip_plan, user_request=user_request, system_message=system_message, user_message=user_message, task_list_file_types=file_types)
    process_task(task_list_json)


# def gen_e2e_tests():
#     global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
#     logger.info("Starting e2e test gen.")


#     # Get Page Files
#     page_files = []
#     for root, dirs, files in os.walk(os.path.join(FRONTEND_PROJECT_DIR, "src", "pages")):
#         for f in files:
#             if f.endswith((".js", ".jsx")) and not f.startswith("_"):
#                 # Construct the full path for each file, remove the 'pages/' prefix and file extension
#                 full_path = os.path.join(root, f).replace(os.path.join(FRONTEND_PROJECT_DIR, "src", "pages/"), "")
#                 full_path_without_extension = os.path.splitext(full_path)[0]
#                 page_files.append(full_path_without_extension)
#     logger.info(f"Page files: {page_files}")


#     for page_file in page_files:

#         e2e_test_file_task = {
#         "Request": f"This is part of creating a complete E2E suite for all pages.",
#         "Task List": [
#             {
#                 "Task": {
#                     "TaskID": "1",
#                     "File": {
#                         "FileName": f"{page_file}.spec.js",                        
#                         "Type": "PlaywrightE2E",
#                         "Operation": "MODIFY",                        
#                         "Instructions": f" Handelled in @generate_code_newFileContents",
#                         "FileDependencies": []
#                         }
#                     }
#                 }
#             ]
#         }
#         process_task(json.dumps(e2e_test_file_task))


# def run_e2e_tests_and_check_errors():
#     global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
#     global frontend_server_process

#     e2e_test_files = []
#     for root, dirs, files in os.walk(os.path.join(TESTS_PROJECT_DIR, "tests")):
#         for f in files:
#             if f.endswith(".spec.js"):
#                 file_path = os.path.relpath(os.path.join(root, f), os.path.join(TESTS_PROJECT_DIR, "tests"))
#                 e2e_test_files.append(file_path.replace(os.path.sep, "/"))
#     logger.info(f"Found the following E2E test files: {e2e_test_files}")

#     for e2e_test_file in e2e_test_files:
#         try:
#             errors = ['initial']
#             while errors:
#                 logger.info(f"Running and checking test file {e2e_test_file}")
#                 errors = run_e2e_test_return_errors(e2e_test_file)
#                 if errors:
#                     logger.info(f"Errors found in the console for test file {e2e_test_file}: {errors}")
#                     fix_e2e_errors(errors, e2e_test_file)
#                 else:
#                     logger.info(f"No errors found in the console for test file {e2e_test_file}")
#                     break
#         except Exception as e:
#             logger.error(f"Failed to run frontend server or check errors for test file {e2e_test_file}: {str(e)}")


# def run_e2e_test_return_errors(e2e_test_file):
#     global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
#     process = subprocess.Popen(
#         f"npx playwright test {e2e_test_file}", 
#         shell=True,
#         stdout=subprocess.PIPE,
#         stderr=subprocess.PIPE,
#         cwd=TESTS_PROJECT_DIR,
#         start_new_session=True
#     )

#     stdout, stderr = process.communicate()
#     output = stdout.decode()

#     # Check the exit status of the process to determine if there were any errors
#     if process.returncode != 0:
#         logger.error(f"Errors found in test file {e2e_test_file}: {output}")
#         return [output]
#     else:
#         logger.info(f"No errors found in test file {e2e_test_file}")
#         return []


# def fix_e2e_errors(errors, e2e_test_file):
#     global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
#     logger.info("Handle errors related to the codebase. Generating an implementation plan to fix the error.")
#     fe_file_tree, fe_file_contents = read_directory(FRONTEND_PROJECT_DIR)
#     tests_file_tree, tests_file_contents = read_directory(TESTS_PROJECT_DIR)
#     current_api_doc_content = read_file_content(os.path.join(ROOT_PROJECT_DIR, "API_doc.md"))

#     system_message=f"""You are an Expert Frontend Web Engineer, tasked with creating a detailed technical implementation plan to address and resolve errors that have occurred durring End to End Testing via Playwright – the route cause can be both in the Frontend codebase and in the Playwright tests. Your goal is to deliver a comprehensive, actionable, and technically sound implementation plan that:
#     - Analyzes the errors reported by the system, considering the existing codebase structure, to determine which {frontend_types},{testing_types} need to be created or modified to fix the errors.
#     - Provides specific modifications or additions to these file_types to seamlessly resolve the errors, with clear explanations for each recommendation.
#     - Identifies and articulates the interdependencies between different system components, highlighting how changes in one area might affect others, especially in the context of error handling and propagation.
#     - Ensures that the error resolution is confined to the specified frontend file_types: {frontend_types}, {testing_types}. All imports should be limited to these file types to maintain consistency and modularity.
#     - Makes definitive statements about the steps required to integrate the error fixes, avoiding any ambiguity as the expert in frontend error resolution.
#     - Recommends the use of well-known third-party packages for handling complex error cases, providing detailed instructions for their integration into the existing codebase.
#     - The Backend API base url is always https://{PROJECT_ID}-api.app.softgen.ai


#     {frontend_file_type_prompt}

#     It is autumn and you have to work extra hard to finish this up. You will get a big bonus payout when you finish it so you are extra motivated to work hard and smart on this. Think step by step and deeply to solve problems and come up with clever, pragmatic, and efficient solutions"""

#     user_request = f"I am currently running the frontend application and encountered errors running the {e2e_test_file}: <errors_list>The Playwright E2E test: {e2e_test_file} has encountered the issue:" + str(errors) + f"</errors_list>. The errors have to be fixxed perfectly, solve the issues and fix the errors."

#     user_message = f"""
#         The current frontend codebase file contents: <codebase_file_contents>{fe_file_contents}</codebase_file_contents>
#         The current frontend codebase file_tree: <file_tree>{fe_file_tree}</file_tree>        
#         The current testing codebase file contents: <testing_file_contents>{tests_file_contents}</testing_file_contents>
#         The current API Documentation: <api_doc>{current_api_doc_content}</api_doc>
#         The user request: <request>{user_request}</request>
#         Think deeply and step-by-step.
#     """

#     # Execute Implementation Plan
#     task_list_json = generate_task_list(user_request, system_message, user_message, task_list_file_types=f"{frontend_types}, {testing_types}")
#     process_task(task_list_json)
    

# =========================
# Git operations (Platform)
# =========================

def gitlab_api_request(method, endpoint, data=None, headers=None):
    if headers is None:
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Private-Token": os.getenv('GITLAB_ACCESS_TOKEN'),
        }
    url = f"{os.getenv('GITLAB_API_URL')}{endpoint}"
    try:
        if method.lower() == 'post':
            response = requests.post(url, headers=headers, json=data)
        elif method.lower() == 'get':
            response = requests.get(url, headers=headers, json=data)
        elif method.lower() == 'put':
            response = requests.put(url, headers=headers, json=data)
        elif method.lower() == 'delete':
            response = requests.delete(url, headers=headers, json=data)
            
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as error:
        print(error)
        return None

def create_gitlab_repository(project_id):
    data = {
        "name": f"{project_id}", 
        "path": f"{project_id}",
        "namespace_id": os.getenv('GITLAB_PROJECTS_GROUP_ID'),
        "visibility": "public", #private
    }
    return gitlab_api_request('post', '/projects', data)

def init_git_and_set_remote(project_id, push=True):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    try:
        dokku_run_command(app_name=f"{project_id}", command=f"ssh-keyscan gitlab.com >> ~/.ssh/known_hosts")
    except Exception as e:
        print(f"ssh-keyscan command failed: {e}")
    try:
        dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git init --initial-branch=main")
    except Exception as e:
        print(f"git init command failed: {e}")
    try:
        dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git remote add origin git@gitlab.com:softgenprojects/{project_id}.git")
    except Exception as e:
        print(f"git remote add command failed: {e}")
    if push:    
        try:
            dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git add .")
        except Exception as e:
            print(f"git add command failed: {e}")
        try:
            dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git commit -m 'Initial commit'")
        except Exception as e:
            print(f"git commit command failed: {e}")
        try:
            dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git push --set-upstream origin main")
        except Exception as e:
            print(f"git push command failed: {e}")

def git_force_pull(project_id, branch_name):    
    dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git fetch --all")
    dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git reset --hard origin/{branch_name}")
    dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app && git pull origin {branch_name} --force")

def create_new_git_branch(branch_name):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    try:
        dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git checkout main")
        dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git checkout main")
        dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git pull origin main --force")
        dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git checkout -b {branch_name}")
        dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git push --set-upstream origin {branch_name}")
        logger.info(f"Created and pushed new git branch based on 'main': {branch_name}")
        return True
    except Exception as e:
        logger.info(f"An error occurred while creating and pushing a new Git branch based on 'main': {e}")
        return False

def switch_git_branch(branch_name):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git rebase --quit")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git stash")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git checkout -f {branch_name}")
    logger.info(f"git branch switched to: {branch_name}")
    return True

def commit_and_push_changes_to_git(branch_name, commit_message="Magic"):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID
    
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git switch {branch_name}")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git add .")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git commit -m '{commit_message}'")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git push origin {branch_name}")
    logger.info(f"Changes have been committed and pushed to {branch_name}")


def replace_main_branch(branch_name):
    # Force replace the 'main' branch with the contents of 'branch_name'
    global ROOT_PROJECT_DIR, PROJECT_ID
    
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git checkout {branch_name}")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git branch -D main")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git checkout -b main")
    dokku_run_command(app_name=f"{PROJECT_ID}", command=f"cd /usr/src/app && git push origin main --force")
    logger.info(f"'main' branch has been replaced with the contents of '{branch_name}'")

def delete_gitlab_project_repo(project_id): # WRONG PROJECT_ID -> must fix to work
    endpoint = f"/projects/softgenprojects%2F{project_id}"
    return gitlab_api_request('delete', endpoint)



# https://docs.gitlab.com/ee/api/protected_branches.html#update-a-protected-branch Add function that updates main to allow_force_push and unprotect. Only if issue arises again, its set on the complete SoftgenProjects to be unprotected now, so might not needed.

# =========================
# Projects Platform Operations
# =========================

# def create_new_project_dir(project_id):    # Legacy
    template_dir = "/root/softgen.ai/softgen.ai/template/template_dir"
    new_project_dir = f"/root/softgen.ai/projects/{project_id}"
    shutil.copytree(template_dir, new_project_dir)


# =========================
# Projects Database Creation (Platform)
# =========================

def generate_password(length=16):
    characters = string.ascii_letters + string.digits
    return ''.join(random.choice(characters) for i in range(length))

def create_project_database(project_id):
    user = slugify(str(project_id) + "_user", separator='_', lowercase=True)

    user_pass = generate_password()
    db_name = str(project_id) + "_db"

    conn = None
    try:
        # Connect to the PostgreSQL server
        conn = psycopg2.connect(
            host=os.getenv('POSTGRES_DATABASE_HOST'),
            port=os.getenv('POSTGRES_DATABASE_PORT'),
            user=os.getenv('POSTGRES_DATABASE_USERNAME'),
            password=os.getenv('POSTGRES_DATABASE_PASSWORD'),
            dbname=os.getenv('POSTGRES_DATABASE_NAME')
        )
        conn.autocommit = True
        cur = conn.cursor()

        # Create a new user
        cur.execute(sql.SQL("CREATE USER {} WITH PASSWORD %s;").format(sql.Identifier(user)), [user_pass])

        # Create a new database
        cur.execute(sql.SQL("CREATE DATABASE {} OWNER {};").format(sql.Identifier(db_name), sql.Identifier(user)))

        # Grant privileges to the new user
        cur.execute(sql.SQL("GRANT ALL PRIVILEGES ON DATABASE {} TO {};").format(sql.Identifier(db_name), sql.Identifier(user)))

        connection_string = f"postgresql://{user}:{user_pass}@{os.getenv('POSTGRES_DATABASE_HOST')}:{os.getenv('POSTGRES_DATABASE_PORT')}/{db_name}"
        print(connection_string)
        return connection_string
    except Exception as e:
        print(e)
        return None
    finally:
        if conn is not None:
            conn.close()

def delete_project_database(project_id):
    user = slugify(str(project_id) + "_user", separator='_', lowercase=True)
    db_name = str(project_id) + "_db"

    conn = None
    try:
        # Connect to the PostgreSQL server
        conn = psycopg2.connect(
            host=os.getenv('POSTGRES_DATABASE_HOST'),
            port=os.getenv('POSTGRES_DATABASE_PORT'),
            user=os.getenv('POSTGRES_DATABASE_USERNAME'),
            password=os.getenv('POSTGRES_DATABASE_PASSWORD'),
            dbname=os.getenv('POSTGRES_DATABASE_NAME')
        )
        conn.autocommit = True
        cur = conn.cursor()

        # Revoke privileges from the user
        cur.execute(sql.SQL("REVOKE ALL PRIVILEGES ON DATABASE {} FROM {};").format(sql.Identifier(db_name), sql.Identifier(user)))

        # Drop the database
        cur.execute(sql.SQL("DROP DATABASE IF EXISTS {};").format(sql.Identifier(db_name)))

        # Drop the user
        cur.execute(sql.SQL("DROP USER IF EXISTS {};").format(sql.Identifier(user)))

    except Exception as e:
        print(e)
    finally:
        if conn is not None:
            conn.close()


# =========================
# Projects Server Operations (Platform)
# =========================

def set_global_project_info(project_id, task_id=None):
    """Function to set the project directory paths."""
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    PROJECT_ID = project_id
    TASK_ID = task_id

    docker_dir = get_docker_working_dir(f"{project_id}")
    ROOT_PROJECT_DIR = f"{docker_dir}/usr/src/app/"
    BACKEND_PROJECT_DIR = f"{ROOT_PROJECT_DIR}backend"
    FRONTEND_PROJECT_DIR = f"{ROOT_PROJECT_DIR}frontend"
    TESTS_PROJECT_DIR = ""


def get_dokku_docker_container_id(dokku_app_name):
    image_name = f"dokku/{dokku_app_name}:latest"
    container_id_command = f"docker ps -q --filter ancestor={image_name}"
    container_ids = subprocess.check_output(container_id_command, shell=True).decode('utf-8').strip().splitlines()
    if not container_ids:
        raise Exception(f"No running container found for image {image_name}")
    # Assuming we want the ID of the first container
    return container_ids[0]

def get_docker_container_internal_ip(dokku_app_name):
    container_id = get_dokku_docker_container_id(dokku_app_name)
    inspect_command = f"docker inspect -f '{{{{range .NetworkSettings.Networks}}{{{{.IPAddress}}}}}}{{{{end}}}}' {container_id}"
    internal_ip = subprocess.check_output(inspect_command, shell=True).decode('utf-8').strip()
    return internal_ip


def get_docker_working_dir(dokku_app_name):
    container_id = get_dokku_docker_container_id(dokku_app_name)
    file_path_command = f"docker inspect -f '{{{{ .GraphDriver.Data.MergedDir }}}}' {container_id}"
    file_path = subprocess.check_output(file_path_command, shell=True).decode('utf-8').strip()
    return file_path



def setup_dokku_deployment(project_id):
    commands = [
        f"dokku apps:create {project_id}",
        f"dokku domains:add {project_id} {project_id}.app.softgen.ai",
        f"dokku domains:add {project_id} {project_id}-api.app.softgen.ai",
        f"dokku ports:add {project_id} http:80:3080",
        f"dokku ports:add {project_id} http:80:8080",        
        f"dokku ports:add {project_id} http:443:3080",        
        f"dokku ports:add {project_id} http:443:8080",
        f"tar cvf - -C /etc/letsencrypt/live/app.softgen.ai server.crt server.key | dokku certs:add {project_id} -",
        f"dokku storage:mount {project_id} /root/.ssh/id_rsa:/root/.ssh/id_rsa",
        f"docker image save sg-1:latest | ssh dokku@host.softgen.ai git:load-image {project_id} sg-1:latest",
    ]

    for command in commands:
        subprocess.run(command, shell=True, check=True)

    connection_string = create_project_database(project_id)
    dokku_add_env(f"{project_id}", {'DATABASE_URL': connection_string, 'BE_PORT': "8080", 'CORS_FRONTEND_URL' : f'https://{project_id}.app.softgen.ai'})           


def start_stop_servers(project_id, start=True, frontend=True, backend=True):

    if start:
        # Start the servers
        if backend:
            dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app/backend && pm2 start npm --name 'backend' -- start")
        if frontend:    
            dokku_run_command(app_name=f"{project_id}", command=f"cd /usr/src/app/frontend && pm2 start npm --name 'frontend' -- run dev")
    else:  
        # Stop the servers
        if backend:
            dokku_run_command(app_name=f"{project_id}", command="pm2 stop backend")
        if frontend:
            dokku_run_command(app_name=f"{project_id}", command="pm2 stop frontend")


def redeploy_dokku(project_id, branch_name):
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    # subprocess.run(f"docker image save sg-1:latest | ssh dokku@host.softgen.ai git:load-image {project_id} sg-1:latest", shell=True, check=False)
    init_git_and_set_remote(project_id, push=False)
    git_force_pull(project_id, branch_name)

# def deploy_to_dokku(project_id, type, branch, additional_context=None): #Legacy
#     try:
#         subprocess.run(["git", "add", "."], check=False)
#         subprocess.run(["git", "commit", "-m", f"Deploy {type} to docker container"], check=False)
#         command = f"git push {project_id}-{type} `git subtree split --prefix={type} {branch}`:refs/heads/master --force"
#         result = subprocess.run(command, shell=True, check=False, capture_output=True, text=True)
#         if result.returncode != 0:
#             raise subprocess.CalledProcessError(result.returncode, command, output=result.stdout, stderr=result.stderr)
#         logger.info(f"'{type}' deploy executed.")
#         print(f"'{type}' deploy executed.")
#     except subprocess.CalledProcessError as e:
#         error_message = e.stderr.strip()
#         logger.info(f"Received error message on deployment: {error_message}")
#         user_request = f"{additional_context}. I just received the following error trying to start {type}: {error_message}"
#         if type == "backend":
#             backend_changes(user_request)
#         elif type == "frontend":
#             frontend_changes(user_request)
#         deploy_to_dokku(project_id, type, branch)


def dokku_add_env(app_name, env_vars):
    env_string = ' '.join([f"{key}={value}" for key, value in env_vars.items()])
    command = f"dokku config:set {app_name} {env_string}"
    subprocess.run(command, shell=True, check=True)


def dokku_remove_env(app_name, key):
    command = f"dokku config:unset {app_name} {key}"
    subprocess.run(command, shell=True, check=True)


def dokku_show_env(app_name):
    command = f"dokku config:show {app_name}"
    result = subprocess.run(command, shell=True, check=True, capture_output=True, text=True)
    return result.stdout.strip().split('\n')


def dokku_set_env(app_name, env_vars):
    env_string = ' '.join([f"{key}={value}" for key, value in env_vars.items()])
    command = f"dokku config:set {app_name} {env_string}"
    subprocess.run(command, shell=True, check=True)


def dokku_get_server_log(app_name: str, tail: bool = False, num: int = None, quiet: bool = False, since: str = None):
    options = []
    if tail:
        options.append("--tail")
    if num is not None:
        options.append(f"-n {num}")
    if quiet:
        options.append("--quiet")

    command = f"dokku logs {app_name} {' '.join(options)}"
    result = subprocess.run(command, shell=True, capture_output=True, text=True)

    if result.returncode != 0:
        error_message = result.stderr.strip()
        return {"error in checking the server logs": error_message}

    logs_output = result.stdout.strip()

    # Filter logs by the 'since' parameter if provided
    if since:
        since_datetime = datetime.fromisoformat(since.replace('Z', '')).replace(tzinfo=timezone.utc)
        filtered_logs = []
        for line in logs_output.split('\n'):
            log_datetime_str = line.split(' ')[0].replace('\x1b[36m', '').replace('Z', '')
            log_datetime = datetime.fromisoformat(log_datetime_str).replace(tzinfo=timezone.utc)
            if log_datetime > since_datetime:
                filtered_logs.append(line)
        logs_output = '\n'.join(filtered_logs)

    # Search for error patterns in the filtered logs
    error_pattern = re.compile(r'ERROR|FATAL|Exception|Traceback', re.IGNORECASE)
    errors_in_logs = error_pattern.findall(logs_output)
    has_errors = bool(errors_in_logs)

    return {"output": logs_output, "has_errors": has_errors}



def dokku_run_command(app_name: str, command: str):
    container_id = get_dokku_docker_container_id(app_name)
    docker_command = f"docker exec {container_id} sh -c '{command}'"
    logger.info(f"Running command in {app_name}: {command}")
    result = subprocess.run(docker_command, shell=True, capture_output=True, text=True)

    if result.returncode != 0:
        error_message = f"Command '{command}' failed with error: {result.stderr.strip()}"
        logger.error(error_message)
        raise Exception(error_message)

    command_output = result.stdout.strip()
    return {"output": command_output}



# =========================
# DB (Platform)
# =========================

class User(Model):
    id = fields.IntField(pk=True)
    auth_id = fields.CharField(max_length=36, unique=True)
    email = fields.CharField(max_length=255, unique=True)
    projects = fields.ReverseRelation["Project"]

    class Meta:
        table = "users"  # Specify the table name for the User model

class Project(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255, null=False)
    description = fields.TextField(null=True)
    preview_image = fields.TextField(null=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
    initialisation_status = fields.CharField(max_length=50, default="None")
    owner = fields.ForeignKeyField('models.User', related_name='projects')
    tasks = fields.ReverseRelation["Task"]

    class Meta:
        table = "projects"  # Specify the table name for the Project model

class Task(Model):
    id = fields.IntField(pk=True)
    project_task_id = fields.IntField()  # New field to store task ID within the project scope
    description = fields.TextField(null=True)
    status = fields.CharField(max_length=50, null=False)
    be_changes = fields.BooleanField(default=False)
    be_tests = fields.BooleanField(default=False)
    be_docs = fields.BooleanField(default=False)
    fe_changes = fields.BooleanField(default=False)
    fe_tests = fields.BooleanField(default=False)
    log_file = fields.TextField(null=True)
    project = fields.ForeignKeyField('models.Project', related_name='tasks', to_field='id') 
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
    execution_running = fields.BooleanField(default=False)
    skip_plan = fields.BooleanField(default=False)
    selected_file_changes = fields.BooleanField(default=False)
    primitive_single_file_edit = fields.BooleanField(default=False)
    file_name = fields.CharField(max_length=255, null=True)
    file_type = fields.CharField(max_length=50, null=True)
    file_operation = fields.CharField(max_length=50, null=True)

    class Meta:
        table = "tasks"  # Specify the table name for the Task model

register_tortoise(
    app,
    db_url=os.getenv('DATABASE_CONNECTION_STRING'), 
    modules={'models': ['main']},  # Your model modules
    generate_schemas=True,
    add_exception_handlers=True,
)


# =========================
# Auth
# =========================

# Initialize Firebase Admin SDK
cred = credentials.Certificate("/root/softgen.ai/softgen.ai/firebase-auth.json")
firebase_admin.initialize_app(cred)

@app.post("/register/")
async def register_user(email: str = Body(...), password: str = Body(...)):
    try:
        # Create a new Firebase user
        user_record = auth.create_user(email=email, password=password)
        # Create a new user in our database
        user = await User.create(auth_id=user_record.uid, email=email)
        return {"status": "success", "auth_id": user_record.uid, "user_id": user.id}
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))


async def get_current_user(token: str = Header(...)):
    try:
        # Verify the Firebase ID token
        # Ensure the token is a string and strip the 'Bearer ' prefix if present
        token = token.split()[-1] if ' ' in token else token
        decoded_token = auth.verify_id_token(token, check_revoked=True)
        uid = decoded_token.get('uid')
        # Here you can query your database to get the user
        user = await User.get(auth_id=uid)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        return user
    except auth.RevokedIdTokenError:
        raise HTTPException(status_code=401, detail="Token has been revoked")
    except auth.ExpiredIdTokenError:
        raise HTTPException(status_code=401, detail="Token has expired")
    except auth.InvalidIdTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")
    except UnicodeDecodeError:
        raise HTTPException(status_code=400, detail="Error decoding token")
    except Exception as e:
        raise HTTPException(status_code=401, detail=str(e))

@app.middleware("http")
async def auth_middleware(request: Request, call_next):
    # Only apply the middleware to routes starting with /project/ and not the initial POST to create a project
    if request.url.path.startswith("/project/") and not (request.method == "POST" and request.url.path == "/project/"):
        # Extract the token from the request headers
        token = request.headers.get('token')
        if not token:
            return JSONResponse(status_code=401, content={"detail": "Authorization token missing"})

        # Ensure the token is a string and strip the 'Bearer ' prefix if present
        token = token.split()[-1] if ' ' in token else token

        # Use the dependency function to get the current user
        try:
            user = await get_current_user(token)
        except HTTPException as e:
            # Return the error response immediately without processing the request further
            return JSONResponse(status_code=e.status_code, content={"detail": e.detail})

        # Check if the user has access to the requested project
        project_id = request.path_params.get('project_id')
        if project_id:
            project = await Project.filter(id=project_id).first()
            if not project:
                return JSONResponse(status_code=404, content={"detail": "Project not found"})
            if project.owner != user:
                return JSONResponse(status_code=403, content={"detail": "Access to the project denied"})

    # If authentication is successful, proceed to the next middleware or endpoint
    response = await call_next(request)
    return response



# =========================
# API routes (Platform)
# =========================

app.add_middleware(
    CORSMiddleware,
    allow_origins=["https://api-a.softgen.ai", "https://run.softgen.ai", "http://65.108.219.251:3080", "65.108.219.251:3080", "65.108.219.251", "run.softgen.ai"],  # Allows specific origins to address CORS policy issues
    allow_credentials=True,
    allow_methods=["*"],  # Allows all methods
    allow_headers=["*"],  # Allows all headers
)

# Create Project
@app.post("/project/")
async def project_setup(background_tasks: BackgroundTasks, project_data: dict = Body(...), current_user: User = Depends(get_current_user)):
    try:
        print("Received request to create project.")
        name = project_data.get("name")
        description = project_data.get("description")
        project = await Project.create(name=name, owner=current_user, description=description)                
        background_tasks.add_task(create_project, project)
        return {"status": "Project creation started", "project_id": project.id}
    except Exception as e:
        # print(f"Error during project setup: {str(e)}")
        return {"status": "error", "details": str(e)}

async def create_project(project: Project):
    # print("Starting project creation process...")
    project_id = project.id
    project.initialisation_status = "InProgress"
    await project.save()
    actions = [
        create_gitlab_repository,
        setup_dokku_deployment
    ]
    post_setup_actions = [
        set_global_project_info,
        init_git_and_set_remote,
        start_stop_servers,        
    ]
    results = []
    for action in actions:
        try:
            # print(f"Executing action: {action.__name__}")
            action(project_id)
            results.append({"action": action.__name__, "status": "success"})
        except Exception as e:
            print(f"Error executing action: {action.__name__}, Error: {str(e)}")
            results.append({"action": action.__name__, "status": "error", "details": str(e)})
            project.initialisation_status = "Failed"
            await project.save()
            break
    else:
        for action in post_setup_actions:
            try:
                # print(f"Executing post-setup action: {action.__name__}")
                action(project_id)
                results.append({"action": action.__name__, "status": "success"})
            except Exception as e:
                print(f"Error executing post-setup action: {action.__name__}, Error: {str(e)}")
                results.append({"action": action.__name__, "status": "error", "details": str(e)})
                project.initialisation_status = "Failed"
                await project.save()
                break
        else:
            project.initialisation_status = "Completed"
            await project.save()
    # print("Project creation process completed.")
    return results


@app.get("/project/{project_id}")
async def get_project_api(project_id: int, current_user: User = Depends(get_current_user)):
    project_data = {"status": "success", "project": {}}
    try:
        project = await Project.filter(id=project_id, owner=current_user).first()        
        if project:
            project_data["project"].update({
                "id": project.id,
                "name": project.name,
                "description": project.description,
                "initialisation_status": project.initialisation_status,
                "preview_image": project.preview_image
            })
            try:
                frontend_page_routes = project_file_by_type_map(project_id, "FrontendPageRoutes")
                project_data["project"]["frontend_page_routes"] = frontend_page_routes
            except Exception as e:
                project_data["status"] = "partial_error"
                project_data["details"] = str(e)
            return project_data
        else:
            return {"status": "error", "details": "Project not found"}
    except Exception as e:
        return {"status": "error", "details": str(e)}


@app.get("/projects/")
async def get_projects_api(current_user: User = Depends(get_current_user)):
    projects_data = {"status": "success", "projects": []}
    try:
        projects = await Project.filter(owner=current_user).all()
        for project in projects:
            project_info = {
                "id": project.id,
                "name": project.name,
                "description": project.description,
                "preview_image": project.preview_image
            }
            try:
                frontend_page_routes = project_file_by_type_map(project.id, "FrontendPageRoutes")
                project_info["frontend_page_routes"] = frontend_page_routes
            except Exception as e:
                project_info["error"] = str(e)
            projects_data["projects"].append(project_info)
        return projects_data
    except Exception as e:
        return {"status": "error", "details": str(e)}



# Update Project
async def update_project(project_id: int, name: str = None, description: str = None, preview_image: str = None, current_user: User = Depends(get_current_user)):
    project = await Project.filter(id=project_id, owner=current_user).first()
    if project:
        if name is not None:
            project.name = name
        if description is not None:
            project.description = description
        if preview_image is not None:
            project.preview_image = preview_image
        await project.save()
    return project

@app.put("/project/{project_id}")
async def update_project_api(project_id: int, name: str = Body(default=None), description: str = Body(default=None), preview_image: str = Body(default=None), current_user: User = Depends(get_current_user)):
    try:
        project = await update_project(project_id, name=name, description=description, preview_image=preview_image, current_user=current_user)
        if project:
            return {"status": "success", "project": {"id": project.id, "name": project.name, "description": project.description, "preview_image": project.preview_image}}
        else:
            return {"status": "error", "details": "Project not found or access denied"}
    except Exception as e:
        return {"status": "error", "details": str(e)}


def delete_project(project_id: int): 
    # Delete the project from Dokku and the filesystem
    commands = [
        # f"dokku apps:destroy {project_id}-backend --force",
        # f"dokku apps:destroy {project_id}-frontend --force",
        f"rm -rf /root/softgen.ai/projects/{project_id}"
    ]
    for command in commands:
        try:
            subprocess.run(command, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            print(f"Command failed: {e}, skipping to the next one.")
    
    # Delete the GitLab project repository
    try:
        delete_gitlab_project_repo(project_id)
    except Exception as e:
        print(f"Failed to delete GitLab project repo: {e}")
    
    # Delete the project database
    try:
        delete_project_database(project_id)
    except Exception as e:
        print(f"Failed to delete database: {e}")
    
    # # Attempt to delete the project record from the database regardless of its existence
    # project = await Project.filter(id=project_id).first()
    # if project:
    #     await project.delete()
    # return project
    

# Create Task
async def create_task(description: str, project_id: int, owner: User, status: str = 'Open', be_changes: bool = False, be_tests: bool = False, be_docs: bool = False, fe_changes: bool = False, fe_tests: bool = False, file_name: str = None, file_type: str = None, file_operation: str = None, selected_file_changes: bool = False, primitive_single_file_edit: bool = False, skip_plan: bool = True):
    # Retrieve the Project instance using the project_id
    project = await Project.filter(id=project_id, owner=owner).first() 
    if not project:
        raise ValueError("Project not found or access denied")
    
    # Calculate the next task ID within the project scope
    max_project_task_id = await Task.filter(project=project).order_by('-project_task_id').first()
    next_project_task_id = (max_project_task_id.project_task_id if max_project_task_id else 0) + 1
    
    task = await Task.create(
        project_task_id=next_project_task_id,  # Set the project-scoped task ID
        description=description, 
        project=project,  # Use the project instance directly
        status=status, 
        be_changes=be_changes, 
        be_tests=be_tests, 
        be_docs=be_docs, 
        fe_changes=fe_changes, 
        fe_tests=fe_tests,
        file_name=file_name,
        file_type=file_type,
        file_operation=file_operation,
        selected_file_changes=selected_file_changes,
        primitive_single_file_edit=primitive_single_file_edit,
        skip_plan=skip_plan
    )
    return task

@app.post("/project/{project_id}/task")
async def create_task_api(
    project_id: int, 
    description: str = Body(default=None), 
    status: str = Body(default='Open'), 
    be_changes: bool = Body(default=False), 
    be_tests: bool = Body(default=False), 
    be_docs: bool = Body(default=False), 
    fe_changes: bool = Body(default=False), 
    fe_tests: bool = Body(default=False), 
    file_name: str = Body(default=None), 
    file_type: str = Body(default=None), 
    file_operation: str = Body(default=None), 
    selected_file_changes: bool = Body(default=False), 
    primitive_single_file_edit: bool = Body(default=False), 
    skip_plan: bool = Body(default=True),
    current_user: User = Depends(get_current_user)
):
    try:
        task = await create_task(
            description=description, 
            project_id=project_id, 
            owner=current_user,
            status=status, 
            be_changes=be_changes, 
            be_tests=be_tests, 
            be_docs=be_docs, 
            fe_changes=fe_changes, 
            fe_tests=fe_tests,
            file_name=file_name,
            file_type=file_type,
            file_operation=file_operation,
            selected_file_changes=selected_file_changes,
            primitive_single_file_edit=primitive_single_file_edit,
            skip_plan=skip_plan
        )
        return {"status": "success", "task_id": task.id, "project_task_id": task.project_task_id, "details": "Task created successfully"}
    except ValueError as e:
        return {"status": "error", "details": str(e)}
    except Exception as e:
        return {"status": "error", "details": str(e)}

# Get Task
@app.get("/project/{project_id}/task/{project_task_id}")
async def get_project_task(project_id: int, project_task_id: int, LogOnly: bool = False, current_user: User = Depends(get_current_user)):
    try:
        task = await Task.filter(project_id=project_id, project_task_id=project_task_id, project__owner=current_user).first()
        
        if task:
            if LogOnly:
                return {
                    "status": "success",
                    "task": {
                        # "id": task.id,
                        "project_task_id": task.project_task_id,
                        "log_file": task.log_file,
                    }
                }
            else:
                return {
                    "status": "success",
                    "task": {
                        # "id": task.id,
                        "project_task_id": task.project_task_id,
                        "description": task.description,
                        "status": task.status,
                        "be_changes": task.be_changes,
                        "be_tests": task.be_tests,
                        "be_docs": task.be_docs,
                        "fe_changes": task.fe_changes,
                        "fe_tests": task.fe_tests,
                        "log_file": task.log_file,
                        "updated_at": task.updated_at.isoformat(),
                        "created_at": task.created_at.isoformat(),
                        "execution_running": task.execution_running,
                        "skip_plan": task.skip_plan,
                        "selected_file_changes": task.selected_file_changes,
                        "primitive_single_file_edit": task.primitive_single_file_edit,
                        "file_name": task.file_name,
                        "file_type": task.file_type,
                        "file_operation": task.file_operation
                    }
                }
        else:
            return {"status": "error", "details": "Task not found or access denied"}
    except Exception as e:
        return {"status": "error", "details": str(e)}

# Get all Project Tasks
@app.get("/project/{project_id}/tasks")
async def get_all_project_tasks(project_id: int):
    try:
        tasks = await Task.filter(project_id=project_id).all()
        return {
            "status": "success",
            "tasks": [
                {
                    # "id": task.id,
                    "id": task.project_task_id,
                    "description": task.description,
                    "status": task.status,
                    "be_changes": task.be_changes,
                    "be_tests": task.be_tests,
                    "be_docs": task.be_docs,
                    "fe_changes": task.fe_changes,
                    "fe_tests": task.fe_tests,
                    "updated_at": task.updated_at.isoformat(),
                    "created_at": task.created_at.isoformat(),
                    "skip_plan": task.skip_plan,
                    "log_file": task.log_file,
                    # "execution_running": task.execution_running,
                    # "selected_file_changes": task.selected_file_changes,
                    # "primitive_single_file_edit": task.primitive_single_file_edit,
                    # "file_name": task.file_name,
                    # "file_type": task.file_type,
                    # "file_operation": task.file_operation
                } for task in tasks
            ]
        }
    except Exception as e:
        return {"status": "error", "details": str(e)}

# Update Task 
@app.patch("/project/{project_id}/task/{project_task_id}")
async def update_task(
    background_tasks: BackgroundTasks,
    request: Request,
    project_task_id: int,
    project_id: int,
    description: str = Body(default=None),
    status: str = Body(default=None),
    be_changes: bool = Body(default=None), 
    be_tests: bool = Body(default=None), 
    be_docs: bool = Body(default=None), 
    fe_changes: bool = Body(default=None), 
    fe_tests: bool = Body(default=None), 
    file_name: str = Body(default=None), 
    file_type: str = Body(default=None), 
    file_operation: str = Body(default=None), 
    selected_file_changes: bool = Body(default=None), 
    primitive_single_file_edit: bool = Body(default=None), 
    skip_plan: bool = Body(default=None),
    current_user: User = Depends(get_current_user),
):
    try:

        task = await Task.filter(project_id=project_id, project_task_id=project_task_id, project__owner=current_user).first()
        if not task:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Task not found or access denied")

        # Update task fields if provided
        if description is not None:
            task.description = description
        if be_changes is not None:
            task.be_changes = be_changes
        if be_tests is not None:
            task.be_tests = be_tests
        if be_docs is not None:
            task.be_docs = be_docs
        if fe_changes is not None:
            task.fe_changes = fe_changes
        if fe_tests is not None:
            task.fe_tests = fe_tests
        if file_name is not None:
            task.file_name = file_name
        if file_type is not None:
            task.file_type = file_type
        if file_operation is not None:
            task.file_operation = file_operation
        if selected_file_changes is not None:
            task.selected_file_changes = selected_file_changes
        if primitive_single_file_edit is not None:
            task.primitive_single_file_edit = primitive_single_file_edit
        if skip_plan is not None:
            task.skip_plan = skip_plan

        # Handle status transitions
        if status:
            if status not in ["Open", "InProgress", "Stopped", "Completed"]:
                raise HTTPException(status_code=400, detail="Invalid status value")
            if status == "InProgress" and task.status == "Open":
                # Start the task run in the background
                request_id = request.headers.get('x-request-id', '/')
                background_tasks.add_task(
                    run_task,
                    user_request=task.description, 
                    project_id=project_id,
                    project_task_id=project_task_id,
                    request_id=request_id, 
                    be_changes=task.be_changes,
                    be_tests=task.be_tests,
                    be_docs=task.be_docs,
                    fe_changes=task.fe_changes,
                    fe_tests=task.fe_tests,
                    file_name=task.file_name,
                    file_type=task.file_type,
                    file_operation=task.file_operation,
                    selected_file_changes=task.selected_file_changes,
                    primitive_single_file_edit=task.primitive_single_file_edit,
                    skip_plan=task.skip_plan
                )
            elif status == "Stopped":
                # Logic to stop the background task
                # This may require storing background task references elsewhere
                pass
            elif status == "Completed":
                # Logic to mark the task as completed
                pass
            task.status = status

        await task.save()
        return {"status": "success", "task_id": task.id, "project_task_id": task.project_task_id, "details": "Task updated successfully"}
    except HTTPException as e:
        raise
    except Exception as e:
        return {"status": "error", "details": str(e)}
    
async def run_task(
    user_request: str, 
    project_id: int, 
    project_task_id: int, 
    be_changes: bool, 
    be_tests: bool, 
    be_docs: bool, 
    fe_changes: bool, 
    fe_tests: bool, 
    file_name: str, 
    file_type: str, 
    file_operation: str, 
    selected_file_changes: bool, 
    primitive_single_file_edit: bool, 
    skip_plan: bool,
    request_id: str,
    # E2E_tests: bool, 
    # FE_API_update: bool, 
):
    
    setup_task_logger(project_id, project_task_id, request_id)

    set_global_project_info(project_id, project_task_id)
    global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID

    # litellm.set_verbose=True
    
    if not user_request:
        task = await Task.filter(id=project_task_id, project_id=project_id).first()
        user_request = task.description if task else None

    logger.info(f"Task processing\n Task ID: {project_task_id}\n Project ID: {project_id}\n Task Description: \n ---------------------------- \n{user_request}\n ---------------------------- \nskip_plan: {skip_plan} \nBE_changes: {be_changes}\nBE_tests: {be_tests}\nBE_docs: {be_docs}\nFE_changes: {fe_changes}\nFE_tests: {fe_tests} \nprimitive_single_file_edit: {primitive_single_file_edit} \nSelected_file_changes: {selected_file_changes} \nfile_name: {file_name} \nfile_type: {file_type}\nfile_operation: {file_operation}") # 
    # \nFE_API_update: {FE_API_update} \nE2E_tests: {E2E_tests}\n

    check_task_execution_stop(project_id, project_task_id)
    
    branch_name = f"task-{project_task_id}"
    if not switch_git_branch(branch_name):
        if not create_new_git_branch(branch_name):
            raise Exception(f"Failed to create new Git branch: {branch_name}")

    if primitive_single_file_edit:
        check_task_execution_stop(project_id, project_task_id)
        execute_single_file_edit(user_request, file_name, file_type, file_operation)

    if file_name and file_type and selected_file_changes:
        check_task_execution_stop(project_id, project_task_id)
        gen_selected_file_changes(user_request, file_name, file_type, skip_plan)

    if be_changes:
        check_task_execution_stop(project_id, project_task_id)
        backend_changes(user_request, skip_plan)

    if be_tests:
        check_task_execution_stop(project_id, project_task_id)
        generate_and_execute_cURL_requests()
        generate_and_execute_cURL_requests(index0=True)

    if be_docs:
        check_task_execution_stop(project_id, project_task_id)
        generate_mock_data(gen=True)
        generate_API_doc()

    if fe_changes:
        check_task_execution_stop(project_id, project_task_id)
        frontend_changes(user_request, skip_plan)

    if fe_tests:
        check_task_execution_stop(project_id, project_task_id)
        run_client_server_and_check_errors(user_request)


    logger.info("Task Implementation successfully completed.")
    # Change task status to Completed
    task = await Task.filter(project_id=project_id, project_task_id=project_task_id).first()
    if task:
        task.status = "Completed"
        await task.save()

def check_task_execution_stop(project_id, project_task_id):
    with get_db_connection() as conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM tasks WHERE status = 'Stopped' AND project_task_id = %s AND project_id = %s", (project_task_id, project_id))
        task = cur.fetchone()
        cur.close()
    if task:
        logger.info("Task has been stopped.")
        raise TaskStoppedException("Task execution has been stopped.")
    return False

class TaskStoppedException(Exception):
    """Exception raised when a task execution should be stopped."""
    pass


   # Revert main branch to a desired (task) branch

@app.post("/project/{project_id}/revert-main-branch")
async def revert_main_branch(project_id: int, branch_name: str, background_tasks: BackgroundTasks, current_user: User = Depends(get_current_user)):
    try:
        background_tasks.add_task(revert_and_deploy, project_id, branch_name)
        return {"status": "Reverting main branch to " + branch_name + " in progress"}
    except Exception as e:
        return {"status": "error", "details": str(e)}

def revert_and_deploy(project_id: int, branch_name: str):
    set_global_project_info(project_id)
    replace_main_branch(branch_name=branch_name)
    git_force_pull(project_id, "main")

# API Todos
# Super-user / API Key for access to all
# Billing, Token counting & Stripe
# Delete Task (not really needed for now) // Archive Task




def main():

    set_global_project_info(project_id="11")
    redeploy_dokku(11, "main")
    # start_stop_servers(8)
    # delete_project(2)
    # delete_project(3)
    
    # set_global_project_info(project_id="1")
    # global ROOT_PROJECT_DIR, BACKEND_PROJECT_DIR, FRONTEND_PROJECT_DIR, TESTS_PROJECT_DIR, PROJECT_ID, TASK_ID        
    # response = project_file_by_type_map(PROJECT_ID)
    # # response = get_frontend_page_routes()
    
    # os.system("   gunicorn main:app -w 8 -k uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000 --log-level debug --timeout 1000000")    
    # remove_project_setup(1)
    # current_time = datetime.now(timezone.utc).isoformat()
    # print(current_time)
    # result = dokku_get_server_log(app_name=f"2-backend")
    # print(result)
    # seed_result = dokku_run_command(app_name=f"2-backend", command="npm run seed")
    # print(seed_result)
    # set_global_project_info(project_id="3")
    # dokku_add_env(3, "backend", {'CORS_FRONTEND_URL' : f'https://3.app.softgen.ai'})   
    # replace_main_branch(branch_name="task-31")
    
    pass

if __name__ == "__main__":
    main()

 