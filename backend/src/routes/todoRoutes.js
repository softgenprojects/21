const express = require('express');
const { listTodos, createTodo, getTodoById, updateTodo, deleteTodo, listAllCategories, createCategory, getCategoryById, updateCategory, deleteCategory } = require('@controllers/todoController');

const router = express.Router();

// Todo routes
router.get('/', listTodos);
router.post('/', createTodo);
router.get('/:id', getTodoById);
router.put('/:id', updateTodo);
router.delete('/:id', deleteTodo);

// Category routes
router.get('/categories', listAllCategories);
router.post('/categories', createCategory);
router.get('/categories/:id', getCategoryById);
router.put('/categories/:id', updateCategory);
router.delete('/categories/:id', deleteCategory);

module.exports = router;