const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const getAllTodos = async () => {
  return await prisma.todo.findMany({
    include: { category: true }
  });
};

const createNewTodo = async (data) => {
  return await prisma.todo.create({
    data: data,
    include: { category: true }
  });
};

const getTodoById = async (id) => {
  return await prisma.todo.findUnique({
    where: { id: id },
    include: { category: true }
  });
};

const updateTodoById = async (id, data) => {
  return await prisma.todo.update({
    where: { id: id },
    data: data,
    include: { category: true }
  });
};

const deleteTodoById = async (id) => {
  return await prisma.todo.delete({
    where: { id: id }
  });
};

const getAllCategories = async () => {
  return await prisma.category.findMany();
};

const createCategory = async (data) => {
  return await prisma.category.create({
    data: data
  });
};

const getCategoryById = async (id) => {
  return await prisma.category.findUnique({
    where: { id: id }
  });
};

const updateCategoryById = async (id, data) => {
  return await prisma.category.update({
    where: { id: id },
    data: data
  });
};

const deleteCategoryById = async (id) => {
  return await prisma.category.delete({
    where: { id: id }
  });
};

module.exports = {
  getAllTodos,
  createNewTodo,
  getTodoById,
  updateTodoById,
  deleteTodoById,
  getAllCategories,
  createCategory,
  getCategoryById,
  updateCategoryById,
  deleteCategoryById
};