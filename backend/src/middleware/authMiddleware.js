const jwt = require('jsonwebtoken');

const authenticateUser = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);

    // Check if the user has the necessary role to perform category operations
    if (user.role !== 'admin' && req.path.includes('/categories')) {
      return res.status(403).json({ message: 'Insufficient permissions to access categories' });
    }

    req.user = user;
    next();
  });
};

module.exports = { authenticateUser };